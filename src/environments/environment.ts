// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export const environment = {
  production: true,
  version: 'development',
  pass: '3079c46d17265a64ce2c44515b76a0f5',
  firebase: {
    apiKey: 'AIzaSyA5eWC1POMAyu10vf8oLG-lhQF-twkdlpg',
    authDomain: 'app-conevan.firebaseapp.com',
    databaseURL: 'https://app-conevan.firebaseio.com',
    projectId: 'app-conevan',
    storageBucket: 'app-conevan.appspot.com',
    messagingSenderId: '364327403003'
  }
};
