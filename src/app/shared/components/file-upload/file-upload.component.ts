import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs/Observable';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent {

  finalUrl: string = '';
  showImage = false;
  @Input() imageTitle: string = '';

  @Output() fileUrlChange = new EventEmitter();

  @Input()
  public get fileUrl(): any {
    return this.finalUrl;
  }

  public set fileUrl(val: any) {
    this.finalUrl = val;
    this.fileUrlChange.emit(this.fileUrl);
    if (val) {
      this.downloadURL = this.storage.ref(val).getDownloadURL();
      this.showImage = true;
    }
  }


  // Main task
  task: AngularFireUploadTask;

  // Progress monitoring
  percentage: Observable<number>;

  snapshot: Observable<any>;

  // Download URL
  downloadURL: Observable<string>;

  // State for dropzone CSS toggling
  isHovering: boolean;

  constructor(private storage: AngularFireStorage) { }


  toggleHover(event: boolean) {
    this.isHovering = event;
  }


  startUpload(event: FileList) {
    // The File object
    const file = event.item(0);
    this.showImage = true;

    // Client-side validation example
    if (file.type.split('/')[0] !== 'image') {
      console.error('Tipo de arquivo não suportado :( ');
      return;
    }

    // The storage path
    const path = `livros/${new Date().getTime()}_${file.name}`;

    // Totally optional metadata
    const customMetadata = { title: this.imageTitle };

    // The main task
    this.task = this.storage.upload(path, file, { customMetadata });

    // Progress monitoring
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges();

    // The file's download URL
    this.snapshot
      .subscribe(res => { this.fileUrl = res.metadata.fullPath; console.log(this.finalUrl) });
  }

  // Determines if the upload task is active
  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes
  }

}
