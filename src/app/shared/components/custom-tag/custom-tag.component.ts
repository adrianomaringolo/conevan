import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custom-tag',
  templateUrl: './custom-tag.component.html',
  styleUrls: ['./custom-tag.component.scss']
})
export class CustomTagComponent implements OnInit {

  @Input() id: string;
  @Input() text: string;

  publicosNames = {
    turma1: 'Turma 1',
    turma2: 'Turma 2',
    turma3: 'Turma 3',
    turma4: 'Turma 4',
    turma5: 'Turma 5',
    evangelizadores: 'Evangelizadores',
    pais: 'Pais'
  };

  constructor() { }

  getText(): string {
    return this.publicosNames[this.id];
  }

  ngOnInit() {
  }

  getClass() {
    switch (this.id) {
      case 'turma1':
        return 'bg-pink';
      case 'turma2':
        return 'bg-yellow';
      case 'turma3':
        return 'bg-blue';
      case 'turma4':
        return 'bg-green';
      case 'turma5':
        return 'bg-white';
      case 'pais':
        return 'bg-gray';
      case 'evangelizadores':
        return 'bg-orange';
    }
  }

}
