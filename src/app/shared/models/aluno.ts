export class Responsavel {
  nome = '';
  email = '';
  telefone = '';
  parentesco = '';
}

export class Aluno {
  id = '';
  nome = '';
  turma = '';
  dataNascimento: Date;
  dataInicio: Date;
  observacoes = '';
  responsavel: Responsavel = new Responsavel();

  constructor() {
  }
}
