import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

import { switchMap } from 'rxjs/operators';
import {EmptyObservable} from 'rxjs/observable/EmptyObservable';


import { Observable } from 'rxjs';
import { NotifierService } from 'angular-notifier';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

interface User {
  uid: string;
  firstName?: string,
  email: string;
  photoURL?: string;
  displayName?: string;
  role?: string;
}

@Injectable()
export class AuthService {

  user: Observable<User>;
  loggedUser: User;
  private userDetails: firebase.User = null;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private router: Router,
    private _notifierService: NotifierService, private _spinnerService: Ng4LoadingSpinnerService) {

    //// Get auth data, then get firestore user document || null
    this.user = this.afAuth.authState
    .pipe(
      switchMap(user => {
        this._spinnerService.show();
        if (user) {
          return this.afs.doc<User>(`usuarios/${user.uid}`).valueChanges();
        } else {
          return new EmptyObservable<User>();
        }
      })
    );

    this.user.subscribe(res => {
        this._spinnerService.hide();
        this.loggedUser = res;
        this.saveToLocalStorage(this.loggedUser);
        this.loggedUser.firstName = res.displayName.split(' ')[0];
      }
    );
  }

  isAdmin(): boolean {
    return this.loggedUser ? this.loggedUser.role === 'admin' : false;
  }

  saveToLocalStorage(user) {
    if (user) {
      window.localStorage.setItem('loggedUser', JSON.stringify(user));
    }
  }

  getFromLocalStorage() {
    return JSON.parse(window.localStorage.getItem('loggedUser'));
  }

  googleLogin() {
    return this.oAuthLogin(new firebase.auth.GoogleAuthProvider())
      .then(res => {
        console.log('Logged in');
        this.router.navigate(['/dashboard']);
      })
      .catch((err) => {
        console.log(err);
        if (err['code'] === 'auth/account-exists-with-different-credential') {
          this._notifierService.notify('error', `O email "${err['email']}" está associado ao login pelo Facebook.
            Por favor, tente novamente por esse serviço`);
        }

      });
  }

  // signInWithFacebook() {
  //   return this.afAuth.auth.signInWithPopup(
  //     new firebase.auth.FacebookAuthProvider()
  //   )
  // }

  facebookLogin() {

    return this.oAuthLogin(new firebase.auth.FacebookAuthProvider())
    .then(res => {
      console.log('Logged in');
      this.router.navigate(['/dashboard']);
    })
    .catch((err) => {
      console.log(err);
      if (err['code'] === 'auth/account-exists-with-different-credential') {
        this._notifierService.notify('error', `O email "${err['email']}" está associado ao login pelo Google.
          Por favor, tente novamente por esse serviço`);
      }
    });
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user);
      });
  }


  private updateUserData(user) {
    // Sets user data to firestore on login

    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`usuarios/${user.uid}`);

    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    };

    return userRef.set(data, { merge: true });
  }

  signOut() {
    this.afAuth.auth.signOut().then(() => {
      window.localStorage.removeItem('loggedUser');
      window.location.reload();
    });
  }
}
