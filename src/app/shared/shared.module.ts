import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { AppRoutingModule } from '../app-routing.module';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthGuard } from './auth.guard';
import { InfoModalComponent } from './components/info-modal/info-modal.component';
import { DropZoneDirective } from './directives/drop-zone.directive';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FileSizePipe } from './pipes/file-size.pipe';
import { CustomTagComponent } from './components/custom-tag/custom-tag.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TypesUtilsService } from './helpers/types.utils';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ConnectionService } from 'ng-connection-service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppRoutingModule,
    AngularFireAuthModule,
    NgbModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot(),
    BrowserModule,
    RouterModule,
    NgMultiSelectDropDownModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger', // set defaults here
      confirmText: 'Sim',
      cancelText: 'Não'
    }),
  ],
  declarations: [
    InfoModalComponent,
    DropZoneDirective,
    FileUploadComponent,
    FileSizePipe,
    CustomTagComponent
  ],
  exports: [
    CommonModule,
    RouterModule,
    BrowserModule,
    FormsModule,
    DropZoneDirective,
    FileUploadComponent,
    CustomTagComponent,
    NgbModule,
    Ng4LoadingSpinnerModule,
    NgMultiSelectDropDownModule,
    ConfirmationPopoverModule
  ],
  entryComponents: [
    InfoModalComponent
  ],
  providers: [
    AuthService,
    AuthGuard,
    TypesUtilsService,
    ConnectionService
  ]
})
export class SharedModule { }
