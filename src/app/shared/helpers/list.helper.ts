import { AuthService } from '../services/auth.service';

export class ListHelper {

  constructor(protected _authService: AuthService) {}

  hasAdminPermission(): boolean {
    return this._authService.isAdmin();
  }
}
