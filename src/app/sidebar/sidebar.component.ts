import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public collapsedAlunos = true;
  public collapsedCalendario = true;
  public collapsedEvangelizadores = true;
  public collapsedBiblioteca = true;

  constructor(private _authService: AuthService) { }

  hasAdminPermission() {
    return this._authService.isAdmin();
  }

  ngOnInit() {
  }

  signOut() {
    this._authService.signOut();
  }

}
