import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlunosEmailComponent } from './alunos-email.component';

describe('AlunosEmailComponent', () => {
  let component: AlunosEmailComponent;
  let fixture: ComponentFixture<AlunosEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlunosEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlunosEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
