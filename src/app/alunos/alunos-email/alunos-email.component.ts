import { Component, OnInit } from '@angular/core';
import { AlunosService } from '../alunos.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthService } from '../../shared/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { InfoModalComponent } from '../../shared/components/info-modal/info-modal.component';

@Component({
  selector: 'app-alunos-email',
  templateUrl: './alunos-email.component.html',
  styleUrls: ['./alunos-email.component.css']
})
export class AlunosEmailComponent implements OnInit {

  alunos: any[];

  constructor(private _alunosService: AlunosService,
    private _spinnerService: Ng4LoadingSpinnerService, private _router: Router,
    private _authService: AuthService, private _modalService: NgbModal) {

    this._alunosService.changeTurma('');
    this._alunosService.alunos.subscribe(res => {
      this.alunos = res.sort((a, b) => ((a['nome'] < b['nome']) ? -1 : ((a['nome'] > b['nome']) ? 1 : 0)));
      this._spinnerService.hide();
    });

  }

  ngOnInit() {
  }

  showEmails() {
    const modalRef = this._modalService.open(InfoModalComponent, {});
    modalRef.componentInstance.htmlTitle = 'Emails da turma';
    modalRef.componentInstance.htmlData = this.alunos
      .filter(a => a.responsavel.email)
      .map(a => a.responsavel.email).sort().join(', ');
  }

  hasPermission(): boolean {
    return this._authService.isAdmin();
  }


}
