import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlunosInativosListComponent } from './alunos-inativos-list.component';

describe('AlunosInativosListComponent', () => {
  let component: AlunosInativosListComponent;
  let fixture: ComponentFixture<AlunosInativosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlunosInativosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlunosInativosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
