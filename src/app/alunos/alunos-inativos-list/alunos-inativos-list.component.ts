import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AlunosService } from '../alunos.service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-alunos-inativos-list',
  templateUrl: './alunos-inativos-list.component.html',
  styleUrls: ['./alunos-inativos-list.component.css']
})
export class AlunosInativosListComponent implements OnInit {

  alunos: any[];
  selectedTurma = '';

  constructor(private _alunosService: AlunosService,
    private _spinnerService: Ng4LoadingSpinnerService, private _router: Router,
    private _authService: AuthService) {
      this._spinnerService.show();

    this._alunosService.findInativos();
    this._alunosService.alunos.subscribe(res => {
      this.alunos = res.sort((a, b) => ((a['nome'] < b['nome']) ? -1 : ((a['nome'] > b['nome']) ? 1 : 0)));
      this._spinnerService.hide();
    });
  }

  ngOnInit() {

  }

  remove(key) {
    this._alunosService.remove(key);
  }

  changeTurma(turma) {
    this.selectedTurma = turma;
    this._spinnerService.show();
    this._alunosService.changeTurma(turma);
  }

  hasPermission(): boolean {
    return this._authService.isAdmin();
  }


  getTurmaLabel(turma) {
    switch (turma) {
      case 'Turma 1':
      return '<div class="badge badge bg-pink">' + turma + '</div>';
      case 'Turma 2':
      return '<div class="badge badge bg-yellow">' + turma + '</div>';
      case 'Turma 3':
      return '<div class="badge badge bg-blue">' + turma + '</div>';
      case 'Turma 4':
      return '<div class="badge badge bg-green">' + turma + '</div>';
      case 'Turma 5':
      return '<div class="badge badge bg-light-gray">' + turma + '</div>';
      case 'Turma 6':
      return '<div class="badge badge bg-secondary">' + turma + '</div>';

    }
  }

  getIdadeLabel(aluno) {
    if (!aluno.dataNascimento) {
      return '';
    }
    const idade = this.getIdade(aluno);

    if (idade > 16) {
      return '<div class="badge badge-pill badge-outline-danger">' + idade + ' anos</div>';
    }

    return '<div class="badge badge-pill badge-outline-primary">' + idade + ' anos</div>';
  }

  private getIdade(aluno) {
    const hoje = new Date();
    const nascimento = new Date(aluno.dataNascimento);

    let diferencaAnos = hoje.getFullYear() - nascimento.getFullYear();
    if (new Date(hoje.getFullYear(), hoje.getMonth(), hoje.getDate()) <
      new Date(hoje.getFullYear(), nascimento.getMonth(), nascimento.getDate())) {
      diferencaAnos--;
    }
    return diferencaAnos;
  }

  editAluno(id): void {
    this._router.navigate(['alunos/editar/', id]);
  }

}
