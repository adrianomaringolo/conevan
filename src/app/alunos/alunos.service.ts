
import { combineLatest as observableCombineLatest, Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Aluno } from '../shared/models/aluno';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { switchMap, map } from 'rxjs/operators';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthService } from '../shared/services/auth.service';

@Injectable()
export class AlunosService {

  idadesTurmas = [
    { turma: 1, min: 2, max: 4 },
    { turma: 2, min: 5, max: 6 },
    { turma: 3, min: 7, max: 9 },
    { turma: 4, min: 10, max: 12 },
    { turma: 5, min: 13, max: 14 },
    { turma: 6, min: 15, max: 25 }
  ];

  private alunosCollection: AngularFirestoreCollection<any>;
  alunos: Observable<any[]>;

  selectedTurma = 'Turma 1';
  turmaFilter$: BehaviorSubject<any | null> = new BehaviorSubject(null);

  constructor(private afs: AngularFirestore, private _spinnerService: Ng4LoadingSpinnerService) {
    this.alunosCollection = afs.collection<any>('alunos');

    this.turmaFilter$ = new BehaviorSubject({ turma: this.selectedTurma, inativo: false });

    this.alunos = observableCombineLatest(
      this.turmaFilter$
    ).pipe(
      switchMap(([turmaFilter]) =>

        this.afs
          .collection('alunos', ref => {
            let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

            if (turmaFilter) {
              query = query.where('inativo', '==', turmaFilter['inativo']);
              if (turmaFilter['turma']) {
                query = query.where('turma', '==', turmaFilter['turma']);
              }
            }
            return query;
          })
          .snapshotChanges().pipe(
            map(actions => {
              this._spinnerService.show();
              return actions.map(action => {
                const data = action.payload.doc.data();
                const id = action.payload.doc.id;
                return { id, ...data };
              });
            })
          )
      )
    );
  }

  add(aluno): Promise<any> {
    return this.alunosCollection.add(aluno);
  }

  remove(key: string): Promise<any> {
    let itemDoc: AngularFirestoreDocument<any>;
    itemDoc = this.afs.doc<any>('alunos/' + key);
    return itemDoc.delete();
  }

  update(aluno): Promise<void | any> {
    const id = aluno['id'];
    delete aluno['id'];

    return this.alunosCollection.doc(id).update(aluno);
  }

  findInativos() {
    this.turmaFilter$.next({ turma: '', inativo: true });
  }

  changeTurma(turma, inativo = false) {
    this.selectedTurma = turma;
    this.turmaFilter$.next({ turma: turma, inativo: inativo });
  }

  getAluno(id): Observable<any> {
    return this.alunosCollection.doc(id).valueChanges();
  }
}
