import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { AlunosService } from '../alunos.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InfoModalComponent } from '../../shared/components/info-modal/info-modal.component';
import { DatePipe } from '@angular/common';

import * as jsPDF from 'jspdf';
import { GenericFunctionsHelper } from 'src/app/shared/helpers/generic-functions.helper';

@Component({
  selector: 'app-alunos-list',
  templateUrl: './alunos-list.component.html',
  styleUrls: ['./alunos-list.component.css']
})
export class AlunosListComponent implements OnInit {

  alunos: any[];
  selectedTurma = 'Turma 1';
  closeResult: string;

  constructor(private _notifierService: NotifierService, private _alunosService: AlunosService,
    private _spinnerService: Ng4LoadingSpinnerService, private _router: Router,
    private _authService: AuthService, private _modalService: NgbModal, private _datePipe: DatePipe,
    private _activateRoute: ActivatedRoute) {

    this._spinnerService.show();

    this._activateRoute.params.subscribe(
      params => {
        if (params['turma']) {
          this.selectedTurma = 'Turma ' + params['turma'];
          localStorage.setItem('selectedTurma', params['turma']);
        } else {
          const turma = localStorage.getItem('selectedTurma');
          if (turma) {
            this.selectedTurma = 'Turma ' + turma;
          }
        }

        this._alunosService.changeTurma(this.selectedTurma);
        this._alunosService.alunos.subscribe(res => {
          this.alunos = res.sort((a, b) => ((a['nome'] < b['nome']) ? -1 : ((a['nome'] > b['nome']) ? 1 : 0)));
          this.alunos.map(aluno => aluno.selected = true);
          this._spinnerService.hide();
        });
      });
  }

  ngOnInit() {
    GenericFunctionsHelper.changeOrientationToPrint('landscape');
  }

  remove(key) {
    this._alunosService.remove(key);
  }

  editAluno(id): void {
    this._router.navigate(['alunos/editar/', id]);
  }

  changeTurma(turma) {
    this._router.navigate(['/alunos/' + turma]);
    this._alunosService.changeTurma('Turma ' + turma);
  }

  getTurmaLabel(turma) {
    switch (turma) {
      case 'Turma 1':
      return '<div class="badge badge bg-pink">' + turma + '</div>';
      case 'Turma 2':
      return '<div class="badge badge bg-yellow">' + turma + '</div>';
      case 'Turma 3':
      return '<div class="badge badge bg-blue">' + turma + '</div>';
      case 'Turma 4':
      return '<div class="badge badge bg-green">' + turma + '</div>';
      case 'Turma 5':
      return '<div class="badge badge bg-light-gray">' + turma + '</div>';
      case 'Turma 6':
      return '<div class="badge badge bg-secondary">' + turma + '</div>';
    }
  }

  hasPermission(): boolean {
    return this._authService.isAdmin();
  }

  getIdadeLabel(aluno) {
    if (!aluno.dataNascimento) {
      return '';
    }
    const idade = this.getIdade(aluno);
    const turmaAluno = aluno.turma.replace('Turma ', '');

    const turma = this._alunosService.idadesTurmas.find(i => i.turma == turmaAluno);

    if (idade < turma.min) {
      return '<div class="badge badge-pill badge-outline-warning">' + idade + ' anos</div>';
    } else {
      if (idade > turma.max) {
        return '<div class="badge badge-pill badge-outline-danger">' + idade + ' anos</div>';
      }
    }

    return '<div class="badge badge-pill badge-outline-primary">' + idade + ' anos</div>';
  }

  private getIdade(aluno) {
    const hoje = new Date();
    const nascimento = new Date(aluno.dataNascimento);

    let diferencaAnos = hoje.getFullYear() - nascimento.getFullYear();
    if (new Date(hoje.getFullYear(), hoje.getMonth(), hoje.getDate()) <
      new Date(hoje.getFullYear(), nascimento.getMonth(), nascimento.getDate())) {
      diferencaAnos--;
    }
    return diferencaAnos;
  }

  drawLabel(doc, aluno, index) {
    const tagSizes = { width: 8.9, height: 3.9 };
    const x = index % 2;
    const y = Math.floor(index / 2);

    doc.rect((x * tagSizes.width) + 1, (y * tagSizes.height) + 1, tagSizes.width, tagSizes.height);
    doc.setTextColor('#000058');

    doc.setFontSize(12);
    doc.text(aluno.turma, (x * tagSizes.width) + 1.5, (y * tagSizes.height) + 1.5,
      { maxWidth: tagSizes.width, baseline: 'top'});

    doc.setFontSize(30);
    doc.text(aluno.nome, (x * tagSizes.width) + 1.5, (y * tagSizes.height) + 2.3,
      { maxWidth: tagSizes.width, baseline: 'top'});

    doc.setFontSize(13);
    doc.text(aluno.sobrenome, (x * tagSizes.width) + 1.5, (y * tagSizes.height) + 3.5,
      { maxWidth: tagSizes.width, baseline: 'top'});
  }

  getLabels() {

    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'cm',
      format: 'a4'
    });

    doc.setLineWidth(0.05);
    doc.setDrawColor(211, 211, 211);

    this.alunos
      .filter(aluno => aluno.selected)
      .forEach((aluno, index) => {
        this.drawLabel(doc, aluno, index);
      });

    doc.save(this.alunos[0].turma + '.pdf');
  }

  openDetails(aluno) {
    const modalRef = this._modalService.open(InfoModalComponent, { });
    modalRef.componentInstance.htmlTitle = '<i class="mdi mdi-account-details"></i> Detalhes do aluno';
    modalRef.componentInstance.htmlData = `<p>${this.getTurmaLabel(aluno.turma)}</p>
      <p><span class="font-weight-bold">Nome</span><br/>${aluno.nome} ${aluno.sobrenome}</p>
      <p><span class="font-weight-bold">Data de Nascimento</span><br/>${
        aluno.dataNascimento ?
          this._datePipe.transform(aluno.dataNascimento, 'dd/MM/yyyy') + ' (' + this.getIdade(aluno) + ' anos)' : '-'}</p>
      <p><span class="font-weight-bold">Observações</span><br/>${aluno.observacoes ? aluno.observacoes : '-'}</p>
      <p class="border-bottom pb-2"><span class="font-weight-bold">Data de Início</span><br/>${
        aluno.dataInicio ?
          this._datePipe.transform(aluno.dataInicio, 'dd/MM/yyyy') : '-'}</p>
      <p><span class="font-weight-bold">Responsável</span></p>
      <p><span class="font-weight-bold">Nome</span><br/>${aluno.responsavel.nome ?
        aluno.responsavel.nome + ' (' + aluno.responsavel.parentesco + ')' : '-'}</p>
      <p><span class="font-weight-bold">Telefone</span><br/>${aluno.responsavel.telefone ? aluno.responsavel.telefone : '-'}</p>
      <p><span class="font-weight-bold">Email</span><br/>${aluno.responsavel.email ? aluno.responsavel.email : '-'}</p>
    `;
  }
}
