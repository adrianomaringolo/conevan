import { Component, OnInit } from '@angular/core';
import { AlunosService } from '../alunos.service';
import { NotifierService } from 'angular-notifier';
import { Router, ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-alunos-form',
  templateUrl: './alunos-form.component.html',
  styleUrls: ['./alunos-form.component.css']
})
export class AlunosFormComponent implements OnInit {

  selectedTurma = 1;

  public aluno: any = {
    nome: '',
    turma: '',
    dataNascimento: null,
    dataInicio: null,
    observacoes: '',
    inativo: false,
    inativoObs: '',
    inativoAt: null,
    createdAt: new Date(),
    responsavel: {
      nome: '',
      email: '',
      telefone: '',
      parentesco: '',
      trabalhador: false
    }
  };

  constructor(private _alunosService: AlunosService, private _notifierService: NotifierService,
    private _router: Router, private _spinnerService: Ng4LoadingSpinnerService,
    private _activateRoute: ActivatedRoute) {
    this._activateRoute.params.subscribe(
      params => {

        if (params['id']) {
          this._spinnerService.show();
          this._alunosService.getAluno(params['id']).subscribe(res => {
            this.aluno = res;
            this.aluno.inativo = this.aluno.inativo || false;
            this.aluno.id = params['id'];

            this._spinnerService.hide();
          });
        }
      });
  }

  ngOnInit() { }

  validate() {
    if (!this.aluno.nome || !this.aluno.turma) {
      this._notifierService.notify('error', 'Preencha todos os campos obrigatórios!');
      return false;
    }
    return true;
  }

  saveAluno() {

    if (!this.validate()) {
      return;
    }

    this._spinnerService.show();

    if (this.aluno.inativo && !this.aluno.inativoAt) {
      this.aluno.inativoAt = new Date().toISOString();
    }

    if (this.aluno && this.aluno.id) {
      this._alunosService.update(this.aluno).then(res => {
        this._spinnerService.hide();
        this._notifierService.notify('success', 'Aluno alterado com sucesso!');
        this._router.navigate(['/alunos']);
      });
    } else {
      this._alunosService.add(this.aluno)
        .then(() => {
          this._spinnerService.hide();
          this._notifierService.notify('success', 'Aluno incluído com sucesso!');
          this._router.navigate(['/alunos']);
        });
    }
  }

}
