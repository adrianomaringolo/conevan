import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { EvangelizadoresService } from '../evangelizadores.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { InfoModalComponent } from '../../shared/components/info-modal/info-modal.component';
import * as jsPDF from 'jspdf';

@Component({
  selector: 'app-evangelizadores-list',
  templateUrl: './evangelizadores-list.component.html',
  styleUrls: ['./evangelizadores-list.component.css']
})
export class EvangelizadoresListComponent implements OnInit {

  evangelizadores: any[];
  selectedAtividade = 'Todas';
  atividades = [];

  constructor(private _notifierService: NotifierService, private _evangelizadoresService: EvangelizadoresService,
    private _spinnerService: Ng4LoadingSpinnerService, private _router: Router,
    private _authService: AuthService, private _modalService: NgbModal, private _datePipe: DatePipe) {

    this.changeAtividade(this.selectedAtividade);

    this._spinnerService.show();
    this.atividades = this._evangelizadoresService.atividades;

    this._evangelizadoresService.evangelizadores.subscribe(res => {
      this.evangelizadores = res.sort((a, b) => ((a['nome'] < b['nome']) ? -1 : ((a['nome'] > b['nome']) ? 1 : 0)));
      this._spinnerService.hide();
    });
  }

  ngOnInit() {

  }

  remove(key) {
    this._evangelizadoresService.remove(key);
  }

  hasPermission(): boolean {
    return this._authService.isAdmin();
  }

  edit(id): void {
    this._router.navigate(['evangelizadores/editar/', id]);
  }

  changeAtividade(atividade) {
    this._evangelizadoresService.changeAtividade(atividade);
  }

  getStyleAtividade(atividade) {
    switch (atividade) {
      case 'Coordenação':
      return 'badge-primary';
      case 'Turma 1':
      return 'bg-pink';
      case 'Turma 2':
      return 'bg-yellow';
      case 'Turma 3':
      return 'bg-blue';
      case 'Turma 4':
      return 'bg-green';
      case 'Turma 5':
      return 'bg-light-gray';
      case 'Apoio Turma 1':
      return 'badge-outline-danger';
      case 'Apoio Turma 2':
      return 'badge-outline-warning';
      case 'Apoio Turma 3':
      return 'badge-outline-info';
      case 'Apoio Externo':
      return 'badge-success';
      case 'Substituição em aula':
      return 'bg-light-gray';
      case 'Organização material':
      return 'badge-outline-info';
      case 'Música':
      return 'badge-dark';
      case 'Artesanato':
      return 'badge-info';
      case 'Reuniões com pais':
      return 'badge-danger';
      case 'Biblioteca':
      return 'badge-secondary text-black';
      case 'Conversa com jovens/Mocidade':
      return 'badge-warning';
    }
  }

  openDetails(evangelizador) {
    const modalRef = this._modalService.open(InfoModalComponent, { });
    modalRef.componentInstance.htmlTitle = '<i class="mdi mdi-account-details"></i> Detalhes do evangelizador';
    modalRef.componentInstance.htmlData = `<p><div class="badge badge
      ${this.getStyleAtividade(evangelizador.atividade)}">${evangelizador.atividade }</div></p>
      <p><span class="font-weight-bold">Nome</span><br/>${evangelizador.nome}</p>
      <p><span class="font-weight-bold">Observações</span><br/>${evangelizador.observacoes ? evangelizador.observacoes : '-'}</p>
      <p class="border-bottom pb-2"><span class="font-weight-bold">Data de Início</span><br/>${
        evangelizador.dataInicio ?
          this._datePipe.transform(evangelizador.dataInicio, 'dd/MM/yyyy') : '-'}</p>
      <p><span class="font-weight-bold">Telefone</span><br/>${evangelizador.telefone ? evangelizador.telefone : '-'}</p>
      <p><span class="font-weight-bold">Email</span><br/>${evangelizador.email ? evangelizador.email : '-'}</p>
      <p><span class="font-weight-bold">Trabalho Morada</span><br/>${ evangelizador.trabalhoMesa ? evangelizador.trabalhoMesa : '-'
      } / ${ evangelizador.trabalhoAtend ? evangelizador.trabalhoAtend : '-' }</p>
    `;
  }

  drawLabel(doc, evang, index) {
    const tagSizes = { width: 8.9, height: 3.9 };
    const x = index % 2;
    const y = Math.floor(index / 2);

    doc.rect((x * tagSizes.width) + 1, (y * tagSizes.height) + 1, tagSizes.width, tagSizes.height);
    doc.setTextColor('#600E0E');

    doc.setFontSize(12);
    doc.text(evang.atividade, (x * tagSizes.width) + 1.5, (y * tagSizes.height) + 1.5,
      { maxWidth: tagSizes.width, baseline: 'top'});

    doc.setFontSize(30);
    doc.text(evang.nome, (x * tagSizes.width) + 1.5, (y * tagSizes.height) + 2.3,
      { maxWidth: tagSizes.width, baseline: 'top'});

    doc.setFontSize(13);
    doc.text(evang.sobrenome ? evang.sobrenome :  '', (x * tagSizes.width) + 1.5, (y * tagSizes.height) + 3.5,
      { maxWidth: tagSizes.width, baseline: 'top'});
  }

  getLabels() {

    const doc = new jsPDF({
      orientation: 'portrait',
      unit: 'cm',
      format: 'a4'
    });

    doc.setLineWidth(0.05);
    doc.setDrawColor(211, 211, 211);

    this.evangelizadores.forEach((evang, index) => {
      this.drawLabel(doc, evang, index);
    });

    doc.save('Evangelizadores-' + this.selectedAtividade + '.pdf');
  }

  showEmails() {
    const modalRef = this._modalService.open(InfoModalComponent, {});
    modalRef.componentInstance.htmlTitle = 'Emails dos evangelizadores';
    modalRef.componentInstance.htmlData = this.evangelizadores
      .filter(a => a.email)
      .map(a => a.email).sort().join(', ');
  }

}
