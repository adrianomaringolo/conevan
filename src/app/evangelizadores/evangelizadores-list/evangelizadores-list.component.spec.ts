import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvangelizadoresListComponent } from './evangelizadores-list.component';

describe('EvangelizadoresListComponent', () => {
  let component: EvangelizadoresListComponent;
  let fixture: ComponentFixture<EvangelizadoresListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvangelizadoresListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvangelizadoresListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
