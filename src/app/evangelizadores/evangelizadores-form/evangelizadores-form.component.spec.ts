import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvangelizadoresFormComponent } from './evangelizadores-form.component';

describe('EvangelizadoresFormComponent', () => {
  let component: EvangelizadoresFormComponent;
  let fixture: ComponentFixture<EvangelizadoresFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvangelizadoresFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvangelizadoresFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
