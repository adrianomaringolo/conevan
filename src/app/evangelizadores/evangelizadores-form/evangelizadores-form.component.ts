import { Component, OnInit } from '@angular/core';
import { EvangelizadoresService } from '../evangelizadores.service';
import { NotifierService } from 'angular-notifier';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-evangelizadores-form',
  templateUrl: './evangelizadores-form.component.html',
  styleUrls: ['./evangelizadores-form.component.css']
})
export class EvangelizadoresFormComponent implements OnInit {

  selectedTurma = 1;

  atividades = this._evangelizadoresService.atividades;

  public evangelizador: any = {
    nome: '',
    dataInicio: null,
    observacoes: '',
    inativo: false,
    inativoAt: null,
    createdAt: new Date(),
    email: '',
    telefone: '',
    atividade: '',
    trabalhoMesa: '',
    trabalhoAtend: ''
  };

  constructor(private _evangelizadoresService: EvangelizadoresService, private _notifierService: NotifierService,
    private _router: Router, private _spinnerService: Ng4LoadingSpinnerService,
    private _activateRoute: ActivatedRoute) {
    this._activateRoute.params.subscribe(
      params => {

        if (params['id']) {
          this._spinnerService.show();
          this._evangelizadoresService.getEvangelizador(params['id']).subscribe(res => {
            this.evangelizador = res;
            this.evangelizador.inativo = this.evangelizador.inativo || false;
            this.evangelizador.id = params['id'];

            this._spinnerService.hide();
          });
        }
      });
  }

  ngOnInit() { }

  validate() {
    if (!this.evangelizador.nome || !this.evangelizador.atividade) {
      this._notifierService.notify('error', 'Preencha todos os campos obrigatórios!');
      return false;
    }
    return true;
  }

  saveEvangelizador() {

    if (!this.validate()) {
      return;
    }

    this._spinnerService.show();

    if (this.evangelizador.inativo && !this.evangelizador.inativoAt) {
      this.evangelizador.inativoAt = new Date().toISOString();
    }

    if (this.evangelizador && this.evangelizador.id) {
      this._evangelizadoresService.update(this.evangelizador).then(res => {
        this._spinnerService.hide();
        this._notifierService.notify('success', 'Evangelizador alterado com sucesso!');
        this._router.navigate(['/evangelizadores']);
      });
    } else {
      this._evangelizadoresService.add(this.evangelizador)
        .then(() => {
          this._spinnerService.hide();
          this._notifierService.notify('success', 'Evangelizador incluído com sucesso!');
          this._router.navigate(['/evangelizadores']);
        });
    }
  }

}
