
import {combineLatest as observableCombineLatest,  Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Aluno } from '../shared/models/aluno';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { switchMap, map } from 'rxjs/operators';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthService } from '../shared/services/auth.service';

@Injectable()
export class EvangelizadoresService {

  private evangelizadoresCollection: AngularFirestoreCollection<any>;
  evangelizadores: Observable<any[]>;

  atividades = [
    'Coordenação',
    'Turma 1',
    'Turma 2',
    'Turma 3',
    'Turma 4',
    'Turma 5',
    'Apoio Turma 1',
    'Apoio Turma 2',
    'Apoio Turma 3',
    'Apoio Externo',
    'Substituição em aula',
    'Organização material',
    'Música',
    'Artesanato',
    'Reuniões com pais',
    'Biblioteca',
    'Conversa com jovens'
  ].sort();

  selectedAtividade = 'Todas';
  evangFilter$: BehaviorSubject<any | null> = new BehaviorSubject(null);

  constructor(private afs: AngularFirestore, private _spinnerService: Ng4LoadingSpinnerService) {
    this.evangelizadoresCollection = afs.collection<any>('evangelizadores');

    this.evangFilter$ = new BehaviorSubject({ inativo: false, atividade: this.selectedAtividade });

    this.evangelizadores = observableCombineLatest(
      this.evangFilter$
    ).pipe(
      switchMap(([evangFilter]) =>

        this.afs
          .collection('evangelizadores', ref => {
            let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

            if (evangFilter) {
              query = query.where('inativo', '==', evangFilter['inativo']);

              if (evangFilter['atividade'] && evangFilter['atividade'] !== 'Todas') {
                query = query.where('atividade', '==', evangFilter['atividade']);
              }
            }
            return query;
          })
          .snapshotChanges().pipe(
            map(actions => {
              this._spinnerService.show();
              return actions.map(action => {
                const data = action.payload.doc.data();
                const id = action.payload.doc.id;
                return { id, ...data };
              });
            })
          )
      )
    );
  }

  add(aluno): Promise<any> {
    return this.evangelizadoresCollection.add(aluno);
  }

  remove(key: string): Promise<any> {
    let itemDoc: AngularFirestoreDocument<any>;
    itemDoc = this.afs.doc<any>('evangelizadores/' + key);
    return itemDoc.delete();
  }

  update(aluno): Promise<void | any> {
    const id = aluno['id'];
    delete aluno['id'];

    return this.evangelizadoresCollection.doc(id).update(aluno);
  }

  changeAtividade(novaAtividade) {
    this.evangFilter$.next({ inativo: false, atividade: novaAtividade });
  }

  findInativos() {
    this.evangFilter$.next({ inativo: true });
  }

  getEvangelizador(id): Observable<any> {
    return this.evangelizadoresCollection.doc(id).valueChanges();
  }
}
