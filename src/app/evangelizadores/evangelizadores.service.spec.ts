import { TestBed, inject } from '@angular/core/testing';

import { EvangelizadoresService } from './evangelizadores.service';

describe('EvangelizadoresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EvangelizadoresService]
    });
  });

  it('should be created', inject([EvangelizadoresService], (service: EvangelizadoresService) => {
    expect(service).toBeTruthy();
  }));
});
