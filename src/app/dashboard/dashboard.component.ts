import { Component } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AlunosService } from '../alunos/alunos.service';
import { EvangelizadoresService } from '../evangelizadores/evangelizadores.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['../app.component.scss','./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent {

  alunos: any[] = [];
  evangelizadores: any[] = [];

  constructor(private _alunosService: AlunosService, private _evangelizadoresService: EvangelizadoresService,
    private _spinnerService: Ng4LoadingSpinnerService) {

    this._spinnerService.show();
    this._alunosService.changeTurma('');
    this._evangelizadoresService.changeAtividade('Todas');

    this._alunosService.alunos.subscribe(res => {
      this.alunos = res;
      this._spinnerService.hide();
    });

    this._evangelizadoresService.evangelizadores.subscribe(res => {
      this.evangelizadores = res;
      this._spinnerService.hide();
    });
  }

}
