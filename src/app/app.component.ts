import { Component } from '@angular/core';
import { AuthService } from './shared/services/auth.service';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  loggedUser = null;
  // userObserver = null;

  constructor(private _auth: AuthService) {
  }

  getLoggedUser() {
    return this._auth.getFromLocalStorage();
  }
}
