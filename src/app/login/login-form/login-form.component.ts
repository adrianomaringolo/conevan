import { Component } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {

  constructor(private _auth: AuthService) { }

  googleLogin() {
    this._auth.googleLogin();
  }

  facebookLogin() {
    this._auth.facebookLogin();
  }

}
