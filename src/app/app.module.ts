import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AlunosFormComponent } from './alunos/alunos-form/alunos-form.component';
import { AlunosListComponent } from './alunos/alunos-list/alunos-list.component';

import { NotifierModule } from 'angular-notifier';
import { AlunosService } from './alunos/alunos.service';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';

import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { AlunosInativosListComponent } from './alunos/alunos-inativos-list/alunos-inativos-list.component';
import { CalendariosListComponent } from './calendarios/calendarios-list/calendarios-list.component';
import { CalendariosFormComponent } from './calendarios/calendarios-form/calendarios-form.component';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { SharedModule } from './shared/shared.module';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { EvangelizadoresListComponent } from './evangelizadores/evangelizadores-list/evangelizadores-list.component';
import { EvangelizadoresFormComponent } from './evangelizadores/evangelizadores-form/evangelizadores-form.component';
import { EvangelizadoresService } from './evangelizadores/evangelizadores.service';
import { CalendarioService } from './calendarios/calendario.service';
import { DatePipe } from '@angular/common';
import { AlunosEmailComponent } from './alunos/alunos-email/alunos-email.component';
import { CalendarioViewComponent } from './calendarios/calendario-view/calendario-view.component';
import { GoogleBookApiService } from './livros/google-book-api.service';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { LivrosModule } from './livros/livros.module';
import { BibliotecaService } from './livros/biblioteca.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    DashboardComponent,
    AlunosFormComponent,
    AlunosListComponent,
    AlunosInativosListComponent,
    CalendariosListComponent,
    CalendariosFormComponent,
    LoginFormComponent,
    PrivacyComponent,
    EvangelizadoresListComponent,
    EvangelizadoresFormComponent,
    AlunosEmailComponent,
    CalendarioViewComponent
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    LivrosModule,
    NotifierModule.withConfig({
      position: {
        horizontal: {
          position: 'right'
        },
        vertical: {
          position: 'top'
        }
      }
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    //ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    AlunosService,
    EvangelizadoresService,
    CalendarioService,
    BibliotecaService,
    GoogleBookApiService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
