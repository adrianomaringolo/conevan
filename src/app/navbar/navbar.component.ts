import { Component, OnInit } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../shared/services/auth.service';
import { ConnectionService } from 'ng-connection-service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  providers: [NgbDropdownConfig]
})
export class NavbarComponent implements OnInit {
  public sidebarOpened = false;
  public loggedUser = null;
  public ano = new Date().getFullYear();
  public semestre = new Date().getMonth() > 6 ? 2 : 1;

  status = 'ONLINE';
  isConnected = true;

  constructor(config: NgbDropdownConfig, private _auth: AuthService, private connectionService: ConnectionService) {
    config.placement = 'bottom-right';
    this.loggedUser = this._auth.getFromLocalStorage();

      this.connectionService.monitor().subscribe(isConnected => {
        this.isConnected = isConnected;
        if (this.isConnected) {
          this.status = 'ONLINE';
        } else {
          this.status = 'OFFLINE';
        }
      });

  }

  ngOnInit() {
  }

  signOut() {
    this._auth.signOut();
    window.localStorage.clear();
  }

  toggleOffcanvas() {
    this.sidebarOpened = !this.sidebarOpened;
    if (this.sidebarOpened) {
      document.querySelector('.sidebar-offcanvas').classList.add('active');
    } else {
      document.querySelector('.sidebar-offcanvas').classList.remove('active');
    }
  }



}
