import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendariosListComponent } from './calendarios-list.component';

describe('CalendariosListComponent', () => {
  let component: CalendariosListComponent;
  let fixture: ComponentFixture<CalendariosListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendariosListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendariosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
