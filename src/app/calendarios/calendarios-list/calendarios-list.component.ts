import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { CalendarioService } from '../calendario.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthService } from '../../shared/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { ListHelper } from 'src/app/shared/helpers/list.helper';

@Component({
  selector: 'app-calendarios-list',
  templateUrl: './calendarios-list.component.html',
  styleUrls: ['./calendarios-list.component.css']
})
export class CalendariosListComponent extends ListHelper implements OnInit {

  calendarios: any[];

  constructor(private _notifierService: NotifierService, private _calendariosService: CalendarioService,
    private _spinnerService: Ng4LoadingSpinnerService, private _router: Router,
    _authService: AuthService, private _datePipe: DatePipe) {

    super(_authService);

    this._spinnerService.show();

    this._calendariosService.calendarios.subscribe(res => {
      this.calendarios = res.sort((a, b) => ((a['ano'] < b['ano']) ? -1 : ((a['ano'] > b['ano']) ? 1 : 0)));
      this._spinnerService.hide();
    });
  }

  ngOnInit() {
  }

  remove(key) {
    this._calendariosService.remove(key);
  }

  view(id) {
    this._router.navigate(['calendarios/visualizar/', id]);
  }

  edit(id): void {
    this._router.navigate(['calendarios/editar/', id]);
  }

  getDataInicio(calendario) {
    return calendario.atividades[0]['data'];
  }

  getDataFim(calendario) {
    return calendario.atividades[calendario.atividades.length - 1]['data'];
  }

  getNumAulas(calendario) {
    return calendario.atividades.filter(atividade => atividade.aula).length;
  }


}
