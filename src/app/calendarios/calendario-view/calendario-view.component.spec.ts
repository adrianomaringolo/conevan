import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarioViewComponent } from './calendario-view.component';

describe('CalendarioViewComponent', () => {
  let component: CalendarioViewComponent;
  let fixture: ComponentFixture<CalendarioViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendarioViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarioViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
