import { Component, OnInit } from '@angular/core';
import { CalendarioService } from '../calendario.service';
import { NotifierService } from 'angular-notifier';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import * as ColorHash from 'color-hash';
import { GenericFunctionsHelper } from 'src/app/shared/helpers/generic-functions.helper';

@Component({
  selector: 'app-calendario-view',
  templateUrl: './calendario-view.component.html',
  styleUrls: ['./calendario-view.component.scss']
})
export class CalendarioViewComponent implements OnInit {

  public calendario: any;
  public meses = [];
  public mode = '';
  public colorHash = new ColorHash({lightness: 0.35, saturation: 0.85});

  labelsMeses = ['janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho',
    'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];

  constructor(private _calendarioService: CalendarioService,
    private sanitizer: DomSanitizer,
    private _router: Router, private _spinnerService: Ng4LoadingSpinnerService,
    private _activateRoute: ActivatedRoute) {
    this._activateRoute.params.subscribe(
      params => {

        if (params['id']) {
          this._spinnerService.show();
          this._calendarioService.getCalendario(params['id']).subscribe(res => {
            this.calendario = res;
            this.calendario.id = params['id'];
            this.groupMeses();
            this._spinnerService.hide();
          });
        }

        if (params['ano'] && params['semestre']) {
          this._spinnerService.show();

          this._calendarioService.calendarios.subscribe(res => {
            this.calendario = res.find(c => c['ano'] == params['ano'] && c['semestre'] == params['semestre']);
            this.mode = 'detail';
            this.getMes(new Date().getUTCMonth());
            GenericFunctionsHelper.changeOrientationToPrint('landscape');
            this._spinnerService.hide();
          });
        }
      });

  }

  getMes(mes) {
    const found = this.calendario.atividades.filter(at => new Date(at.data).getUTCMonth() === mes);
    if (found.length > 0) {
      this.meses.push(found);
    }
  }

  getMesLabel(data) {
    return this.labelsMeses[new Date(data).getUTCMonth()];
  }

  groupMeses() {
    this.labelsMeses.forEach((_, index) => {
      this.meses.push(this.calendario.atividades.filter(at => new Date(at.data).getUTCMonth() === index));
    });
  }

  ngOnInit() {
    GenericFunctionsHelper.changeOrientationToPrint('portrait');
  }

  getDiaAtividade(atividade) {
    return new Date(atividade.data).getUTCDate();
  }

  getDescAula(atividade) {
    if ((atividade.aula && atividade.externa) ||
      (atividade.aula && !atividade.externa)) {
      return 'Aula e ';
    }

    if (!atividade.aula && atividade.externa) {
      return 'Atividade Externa - ';
    }

    if (!atividade.aula && !atividade.externa) {
      return 'Não haverá aula - ';
    }

    return '';
  }

  getDescricoes(atividade) {
    return this.sanitizer.bypassSecurityTrustHtml(
      this.getDescAula(atividade) +
      atividade.descricoes.filter(i => i != '').map(desc => {
        const color = atividade.aula ? this.colorHash.hex(desc.split('(')[0].replace(' ', '')) : '';

        return `<span class="mb-1" style="display:inline;width:45%;color:${color}">${desc}</span>`;
      }).join (' | ')
    );
  }

}
