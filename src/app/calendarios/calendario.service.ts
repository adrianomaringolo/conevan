
import {combineLatest as observableCombineLatest,  Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Aluno } from '../shared/models/aluno';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { switchMap, map } from 'rxjs/operators';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Injectable()
export class CalendarioService {

  private calendariosCollection: AngularFirestoreCollection<any>;
  calendarios: Observable<any[]>;
  calendarioFilter$: BehaviorSubject<any | null> = new BehaviorSubject(null);

  selectedSemestre = { ano: new Date().getFullYear, semestre: 1 };

  constructor(private afs: AngularFirestore, private _spinnerService: Ng4LoadingSpinnerService) {
    this.calendariosCollection = afs.collection<any>('calendarios');

    this.calendarioFilter$ = new BehaviorSubject({ });

    this.calendarios = observableCombineLatest(
      this.calendarioFilter$
    ).pipe(
      switchMap(([calendarioFilter]) =>

        this.afs
          .collection('calendarios', ref => {
            let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

            if (calendarioFilter) {
              if (calendarioFilter['ano']) {
                query = query.where('ano', '==', calendarioFilter['ano']);
              }
              if (calendarioFilter['semestre']) {
                query = query.where('semestre', '==', calendarioFilter['semestre']);
              }
            }
            return query;
          })
          .snapshotChanges().pipe(
            map(actions => {
              this._spinnerService.show();
              return actions.map(action => {
                const data = action.payload.doc.data();
                const id = action.payload.doc.id;
                return { id, ...data };
              });
            })
          )
      )
    );
  }

  add(aluno): Promise<any> {
    return this.calendariosCollection.add(aluno);
  }

  remove(key: string): Promise<any> {
    let itemDoc: AngularFirestoreDocument<any>;
    itemDoc = this.afs.doc<any>('calendarios/' + key);
    return itemDoc.delete();
  }

  update(aluno): Promise<void | any> {
    const id = aluno['id'];
    delete aluno['id'];

    return this.calendariosCollection.doc(id).update(aluno);
  }

  findInativos() {
    this.calendarioFilter$.next({ turma: '', inativo: true });
  }

  getCalendarioByAnoSemestre(pAno, pSemestre) {
    this.calendarioFilter$.next({ ano: pAno, semestre: pSemestre });
  }

  getCalendario(id): Observable<any> {
    return this.calendariosCollection.doc(id).valueChanges();
  }

}
