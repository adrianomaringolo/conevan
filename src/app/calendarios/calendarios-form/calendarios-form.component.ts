import { Component, OnInit } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { CalendarioService } from '../calendario.service';
import { NotifierService } from 'angular-notifier';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-calendarios-form',
  templateUrl: './calendarios-form.component.html',
  styleUrls: ['./calendarios-form.component.css']
})
export class CalendariosFormComponent implements OnInit {

  public calendario: any = {
    ano: '',
    semestre: '',
    createdAt: new Date(),
    atividades: []
  };

  public atividadeDefault: any = Object.freeze({
    aula: true,
    data: null,
    externa: false,
    descricoes: ['']
  });

  constructor(private _calendarioService: CalendarioService,
    private _notifierService: NotifierService,
    private _router: Router, private _spinnerService: Ng4LoadingSpinnerService,
    private _activateRoute: ActivatedRoute) {
      this._activateRoute.params.subscribe(
        params => {

          if (params['id']) {
            this._spinnerService.show();
            this._calendarioService.getCalendario(params['id']).subscribe(res => {
              this.calendario = res;
              this.calendario.id = params['id'];

              this._spinnerService.hide();
            });
          }

          if (params['mes']) {
            this._spinnerService.show();
            this._calendarioService.getCalendario(params['id']).subscribe(res => {
              this.calendario = res;
              this.calendario.id = params['id'];

              this.calendario.atividades = this.calendario.atividades;

              this._spinnerService.hide();
            });
          }
        });
    }

  customTrackBy(index: number, obj: any): any {
    return  index;
  }

  ngOnInit() {
    this.calendario.atividades.push(JSON.parse(JSON.stringify(this.atividadeDefault)));
  }

  addAtividade() {
    this.calendario.atividades.push(JSON.parse(JSON.stringify(this.atividadeDefault)));
  }

  addDescricao(atividade) {
    atividade.descricoes.push('');
  }

  normalizeData() {
    this.calendario.atividades = this.calendario.atividades.filter(at => at['data'] !== '');
    this.calendario.atividades = this.calendario.atividades
      .sort((a, b) => ((a['data'] < b['data']) ? -1 : ((a['data'] > b['data']) ? 1 : 0)));
  }

  saveCalendario() {
    this._spinnerService.show();

    this.normalizeData();

    if (this.calendario && this.calendario.id) {
      this._calendarioService.update(this.calendario).then(res => {
        this._spinnerService.hide();
        this._notifierService.notify('success', 'Calendário alterado com sucesso!');
        this._router.navigate(['/calendarios']);
      });
    } else {
      this._calendarioService.add(this.calendario)
        .then(() => {
          this._spinnerService.hide();
          this._notifierService.notify('success', 'Calendário incluído com sucesso!');
          this._router.navigate(['/calendarios']);
        });
    }
  }

}
