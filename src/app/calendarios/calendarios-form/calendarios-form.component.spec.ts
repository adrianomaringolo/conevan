import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendariosFormComponent } from './calendarios-form.component';

describe('CalendariosFormComponent', () => {
  let component: CalendariosFormComponent;
  let fixture: ComponentFixture<CalendariosFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalendariosFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendariosFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
