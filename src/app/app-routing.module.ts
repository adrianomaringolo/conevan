import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AlunosListComponent } from './alunos/alunos-list/alunos-list.component';
import { AlunosFormComponent } from './alunos/alunos-form/alunos-form.component';
import { AlunosInativosListComponent } from './alunos/alunos-inativos-list/alunos-inativos-list.component';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { AuthGuard } from './shared/auth.guard';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { CalendariosListComponent } from './calendarios/calendarios-list/calendarios-list.component';
import { EvangelizadoresListComponent } from './evangelizadores/evangelizadores-list/evangelizadores-list.component';
import { EvangelizadoresFormComponent } from './evangelizadores/evangelizadores-form/evangelizadores-form.component';
import { CalendariosFormComponent } from './calendarios/calendarios-form/calendarios-form.component';
import { AlunosEmailComponent } from './alunos/alunos-email/alunos-email.component';
import { CalendarioViewComponent } from './calendarios/calendario-view/calendario-view.component';
import { LivrosListComponent } from './livros/livros-list/livros-list.component';
import { LivrosFormComponent } from './livros/livros-form/livros-form.component';
import { LivrosImportComponent } from './livros/livros-import/livros-import.component';
import { LivrosViewComponent } from './livros/livros-view/livros-view.component';
import { LivrosManagementComponent } from './livros/livros-management/livros-management.component';

const routes: Routes = [

  { path: 'login', component: LoginFormComponent },
  { path: 'politica-privacidade', component: PrivacyComponent },

  { path: 'alunos', component: AlunosListComponent, canActivate: [AuthGuard] },
  { path: 'alunos/criar', component: AlunosFormComponent, canActivate: [AuthGuard] },
  { path: 'alunos/editar/:id', component: AlunosFormComponent, canActivate: [AuthGuard] },
  { path: 'alunos/:turma', component: AlunosListComponent, canActivate: [AuthGuard] },
  { path: 'alunos-emails', component: AlunosEmailComponent, canActivate: [AuthGuard] },
  { path: 'alunos-inativos', component: AlunosInativosListComponent, canActivate: [AuthGuard] },


  { path: 'evangelizadores', component: EvangelizadoresListComponent, canActivate: [AuthGuard] },
  { path: 'evangelizadores/criar', component: EvangelizadoresFormComponent, canActivate: [AuthGuard] },
  { path: 'evangelizadores/editar/:id', component: EvangelizadoresFormComponent, canActivate: [AuthGuard] },

  { path: 'calendarios', component: CalendariosListComponent, canActivate: [AuthGuard] },
  { path: 'calendarios/criar', component: CalendariosFormComponent, canActivate: [AuthGuard] },
  { path: 'calendarios/editar/:id', component: CalendariosFormComponent, canActivate: [AuthGuard] },
  { path: 'calendarios/visualizar/:id', component: CalendarioViewComponent, canActivate: [AuthGuard] },
  { path: 'calendarios/visualizar/:ano/:semestre', component: CalendarioViewComponent, canActivate: [AuthGuard] },

  { path: 'livros', component: LivrosListComponent, canActivate: [AuthGuard] },
  { path: 'biblioteca/gerenciar', component: LivrosManagementComponent, canActivate: [AuthGuard] },
  { path: 'biblioteca/emprestimos', component: LivrosManagementComponent, canActivate: [AuthGuard] },
  { path: 'livros/criar', component: LivrosFormComponent, canActivate: [AuthGuard] },
  { path: 'livros/editar/:id', component: LivrosFormComponent, canActivate: [AuthGuard] },
  { path: 'livros/copiar/:id', component: LivrosFormComponent, canActivate: [AuthGuard] },
  { path: 'livros/importar', component: LivrosImportComponent, canActivate: [AuthGuard] },
  { path: 'livros/:id', component: LivrosViewComponent, canActivate: [AuthGuard] },


  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: '', pathMatch: 'full', redirectTo: 'dashboard' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
