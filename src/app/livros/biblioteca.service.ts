
import { combineLatest as observableCombineLatest, Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { switchMap, map, tap } from 'rxjs/operators';

import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

export const TiposSocios = {
  ALUNOS: 'Alunos',
  EVANGELIZADORES: 'Evangelizadores',
  PAIS: 'Pais',
  OUTROS: 'Outros'
};

@Injectable()
export class BibliotecaService {
  constructor(private afs: AngularFirestore, private _spinnerService: Ng4LoadingSpinnerService) {

    this.startTemasCollection();
    this.startSociosCollection();

    this.livrosCollection = afs.collection<any>('livros');
    this.livrosFilter$ = new BehaviorSubject({ orderBy: 'tombo', perPage: 60, status: 'ativo' });

    this.livros = observableCombineLatest(this.livrosFilter$)
      .pipe(
        switchMap(([livrosFilter]) =>

          this.afs
            .collection('livros', ref => {
              let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;

              query = query.orderBy('tombo', 'desc');

              if (livrosFilter.status) {
                query = query.where('status', '==', livrosFilter.status);
              }
              return query;
            })
            .snapshotChanges().pipe(
              map(actions => {
                this._spinnerService.show();
                return this.convertObjectsFromFirebase(actions);
              })
            )
        )
      );
  }


  private livrosCollection: AngularFirestoreCollection<any>;
  livros: Observable<any[]>;
  livrosFilter$: BehaviorSubject<any | null> = new BehaviorSubject(null);

  private temasCollection: AngularFirestoreCollection<any>;
  temas: any[] = [];

  private sociosCollection: AngularFirestoreCollection<any>;
  socios: Observable<any[]>;

  startTemasCollection() {
    this.temasCollection = this.afs.collection<any>('temas');

    this.afs
      .collection('temas', ref => {
        let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
        query = query.orderBy('descricao');
        return query;
      })
      .snapshotChanges().pipe(
        map(actions => {
          this._spinnerService.show();
          return actions.map(action => {
            const data = action.payload.doc.data();
            const id = action.payload.doc.id;
            return { id, ...data };
          });
        })
      ).subscribe(res => {
        this.temas = res;
        console.log(this.temas);
      });
  }

  startSociosCollection() {
    this.sociosCollection = this.afs.collection<any>('socios');

    this.socios = this.afs
      .collection('socios', ref => {
        let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
        query = query.orderBy('nome').where('status', '==', 'ativo');
        return query;
      })
      .snapshotChanges().pipe(
        map(actions => {
          this._spinnerService.show();
          return this.convertObjectsFromFirebase(actions);
        })
      );
  }

  convertObjectsFromFirebase(objects) {
    return objects.map(action => {
      const data = action.payload.doc.data();
      const id = action.payload.doc.id;
      return { id, ...data };
    });
  }

  pad(number, length = 4) {

    const prefix = number.split('-')[0];
    const sufix = number.split('.')[1];
    const currentNumber = number.split('-')[0].split('.')[0];

    let str = '' + currentNumber;
    while (str.length < length) {
      str = '0' + str;
    }
    return (prefix ? prefix + '-' : '') + str + (sufix ? '.' + sufix : '');
  }

  add(livro): Promise<any> {
    livro.tombo = this.pad(livro.tombo);
    livro.status = 'ativo';
    livro.createdAt = new Date();
    livro.updatedAt = new Date();
    return this.livrosCollection.add(livro);
  }

  addTema(tema): Promise<any> {
    tema.createdAt = new Date();
    tema.updatedAt = new Date();
    return this.temasCollection.add(tema);
  }

  addSocio(socio): Promise<any> {
    socio.createdAt = new Date();
    socio.updatedAt = new Date();
    socio.status = 'ativo';
    return this.sociosCollection.add(socio);
  }

  getByTombo(tombo) {
    return this.afs.collection('livros', ref => ref.where('tombo', '==', tombo)).get();
  }

  remove(key: string): Promise<any> {
    let itemDoc: AngularFirestoreDocument<any>;
    itemDoc = this.afs.doc<any>('livros/' + key);
    return itemDoc.delete();
  }

  removeTema(key: string): Promise<any> {
    let itemDoc: AngularFirestoreDocument<any>;
    itemDoc = this.afs.doc<any>('temas/' + key);
    return itemDoc.delete();
  }

  removeSocio(key: string): Promise<any> {
    let itemDoc: AngularFirestoreDocument<any>;
    itemDoc = this.afs.doc<any>('socios/' + key);
    return itemDoc.delete();
  }

  update(livro): Promise<void | any> {
    const id = livro['id'];
    delete livro['id'];
    livro.thumbCapa = livro.thumbCapa ? livro.thumbCapa : '';
    livro.updatedAt = new Date();

    return this.livrosCollection.doc(id).update(livro);
  }

  updateSocio(socio): Promise<void | any> {
    const id = socio['id'];
    delete socio['id'];

    return this.sociosCollection.doc(id).update(socio);
  }

  addReview(review, livro): Promise<void | any> {
    const id = livro['id'];
    delete livro['id'];

    if (!livro['reviews']) {
      livro['reviews'] = [];
    }

    livro['reviews'].push(review);

    return this.livrosCollection.doc(id).update(livro);
  }

  removeReview(review, livro): Promise<void | any> {
    const id = livro['id'];
    delete livro['id'];
    const index = livro.reviews.findIndex(rev => rev.usuario.uid === review.usuario.uid);

    if (!livro['reviews']) {
      livro['reviews'] = [];
    }
    livro['reviews'].splice(index, 1);

    return this.livrosCollection.doc(id).update(livro);
  }


  changeFilters(orderBy, status, searchTombo?, publico?) {
    this.livrosFilter$.next({ orderBy: orderBy, status: status, perPage: 12, searchTombo: searchTombo, publico: publico });
  }

  getLivro(id): Observable<any> {
    return this.livrosCollection.doc(id).valueChanges();
  }
}
