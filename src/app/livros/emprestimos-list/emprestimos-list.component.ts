import { Component, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { DatePipe } from '@angular/common';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ListHelper } from 'src/app/shared/helpers/list.helper';
import { BibliotecaService } from '../biblioteca.service';

@Component({
  selector: 'app-emprestimos-list',
  templateUrl: './emprestimos-list.component.html',
  styleUrls: ['./emprestimos-list.component.scss']
})
export class EmprestimosListComponent extends ListHelper implements OnInit {

  livros: any[];
  livrosBackup: any[];
  temas: any[];

  page = 1;
  pageSize = 30;

  filterOptions = {
    orderBy: 'tombo-desc',
    searchText: '',
    searchTombo: '',
    publico: '',
    tema: ''
  };

  statusLivros = Object.freeze({
    ativo: 'ativo',
    perdido: 'perdido',
    doado: 'doado',
    sem_uso: 'sem_uso'
  });

  constructor(private _livrosService: BibliotecaService,
    private _spinnerService: Ng4LoadingSpinnerService, private _router: Router,
    _authService: AuthService, private _datePipe: DatePipe) {

    super(_authService);

    this._spinnerService.show();

    this.livrosBackup = JSON.parse(localStorage.getItem('livros'));

    if (this.livrosBackup) {
      this.livros = JSON.parse(JSON.stringify(this.livrosBackup));
      this.changeFilter(true);
    } else {
      this._livrosService.livros.subscribe(res => {
        this.groupByTombo(res);
        this._spinnerService.hide();
        this.changeFilter(true);
      });
    }
  }

  ngOnInit() {
    this.findTemas();
  }

  groupByTombo(collection) {
    const newLivros = [];
    collection.forEach(livro => {
      if (livro.tombo.indexOf('.') > -1) { // é um sub-item
        let foundLivro = collection.find(l => l.tombo === livro.tombo.split('.')[0]);
        if (!foundLivro['exemplaresAdicionais']) {
          foundLivro['exemplaresAdicionais'] = [];
        }
        foundLivro['exemplaresAdicionais'].push(livro);
      } else {
        newLivros.push(livro);
      }
    });

    this.livros = newLivros;
    this.livrosBackup = JSON.parse(JSON.stringify(this.livros));

    localStorage.setItem('livros', JSON.stringify(this.livrosBackup));
  }

  changeFilter(isInit = false) {

    this.livros = JSON.parse(JSON.stringify(this.livrosBackup));

    if (!isInit) {
      localStorage.setItem('livrosFilters', JSON.stringify(this.filterOptions));
    } else {
      this.filterOptions = JSON.parse(localStorage.getItem('livrosFilters')) || this.filterOptions;
    }

    switch (this.filterOptions.orderBy) {
      case 'createdAt': {
        this.livros = this.livros.sort((a, b) => a.createdAt - b.createdAt);
        break;
      }
      case 'tombo-asc': {
        this.livros = this.livros.sort((a, b) => a.tombo - b.tombo);
        break;
      }
      case 'titulo': {
        this.livros = this.livros.sort((a, b) => a.titulo - b.titulo);
        break;
      }
      default: {
        this.livros = this.livros.sort((a, b) => b.tombo - a.tombo);
        break;
      }
    }

    if (this.filterOptions.searchTombo) {
      this.livros = this.livros
        .filter(livro => livro.tombo.indexOf(this.filterOptions.searchTombo) >= 0);
    }

    if (this.filterOptions.searchText) {
      this.livros = this.livros
        .filter(livro =>
          livro.tombo.toUpperCase().indexOf(this.filterOptions.searchText.toUpperCase()) >= 0 ||
          livro.titulo.toUpperCase().indexOf(this.filterOptions.searchText.toUpperCase()) >= 0 ||
          livro.autores.toString().toUpperCase().indexOf(this.filterOptions.searchText.toUpperCase()) >= 0);
    }

    if (this.filterOptions.publico) {
      this.livros = this.livros
        .filter(livro => livro.publicos ?
          livro.publicos.find(p => p.id === this.filterOptions.publico) !== undefined :
          false);
    }

    if (this.filterOptions.tema && this.filterOptions.tema.length > 0) {
      this.livros = this.livros
        .filter(livro => livro.temas ?
          livro.temas.find(t => t.descricao === this.filterOptions.tema[0]['descricao']) !== undefined :
          false);
    }
  }


  remove(key) {
    this._livrosService.remove(key);
  }

  edit(id): void {
    this._router.navigate(['livros/editar/', id]);
  }

  findTemas() {
    // this._spinnerService.show();
    // this._livrosService.temas.subscribe(res => {
    //   this.temas = res;
    //   this._spinnerService.hide();
    // });
  }

  getStatusLivro(livro) {
    switch (livro.status) {
      case (this.statusLivros.ativo):
        return '<span class="badge badge-success">Ativo</span>';
      case (this.statusLivros.perdido):
        return '<span class="badge badge-danger">Perdido</span>';
      case (this.statusLivros.doado):
        return '<span class="badge badge-secondary">Doado</span>';
      case (this.statusLivros.ativo):
        return '<span class="badge badge-dark">Sem condições de uso</span>';
      default:
        return '';
    }
  }

  updateCollection() {
    localStorage.removeItem('livros');

    this._livrosService.livros.subscribe(res => {
      this.groupByTombo(res);
      this._spinnerService.hide();
    });
  }

}
