import { Component, OnInit } from '@angular/core';
import { GoogleBookApiService } from '../google-book-api.service';
import { NotifierService } from 'angular-notifier';
import { Router, ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Location } from '@angular/common';
import { BibliotecaService } from '../biblioteca.service';

@Component({
  selector: 'app-livros-form',
  templateUrl: './livros-form.component.html',
  styleUrls: ['./livros-form.component.scss']
})
export class LivrosFormComponent {

  public livro: any = {
    titulo: '',
    colecao: '',
    autores: [],
    isbn: '',
    editora: '',
    anoEdicao: '',
    numeroPaginas: 0,
    descricao: '',
    thumbCapa: '',
    status: 'ativo',
    publicos: [],
    temas: [],
    cadastroCompleto: false
  };

  dropdownListPublicos = [
    { id: 'turma1', descricao: 'Turma 1' },
    { id: 'turma2', descricao: 'Turma 2' },
    { id: 'turma3', descricao: 'Turma 3' },
    { id: 'turma4', descricao: 'Turma 4' },
    { id: 'turma5', descricao: 'Turma 5' },
    { id: 'evangelizadores', descricao: 'Evangelizadores' },
    { id: 'pais', descricao: 'Pais' },
  ];

  dropdownSettings = {
    singleSelection: false,
    textField: 'descricao',
    enableCheckAll: false,
    itemsShowLimit: 3,
    allowSearchFilter: false
  };

  public temas = [];
  public foundLivros = [];
  imageURL = '/assets/images/no-book-bg.png';

  constructor(private _googleApi: GoogleBookApiService,
    public _livrosService: BibliotecaService, private _notifierService: NotifierService,
    private _router: Router, private _spinnerService: Ng4LoadingSpinnerService,
    private _activateRoute: ActivatedRoute, private _location: Location) {

    this._activateRoute.params.subscribe(
      params => {

        if (params['id']) {
          this._spinnerService.show();
          this._livrosService.getLivro(params['id']).subscribe(res => {
            this.livro = res;

            if (window.location.href.indexOf('copiar') === -1) {
              this.livro.id = params['id'];
            }

            this._spinnerService.hide();
          });
        }
      });
  }

  getBookInfos(res) {
    console.log(res['items'][0]);

    this.livro.titulo = this.livro.titulo ? this.livro.titulo :
      res['items'][0]['volumeInfo']['title'] || '';

    this.livro.subtitulo = this.livro.subtitulo ? this.livro.subtitulo :
      res['items'][0]['volumeInfo']['subtitle'] || '';

    this.livro.editora = this.livro.editora ? this.livro.editora :
      res['items'][0]['volumeInfo']['publisher'] || '';

    this.livro.autores = this.livro.autores.length ? this.livro.autores :
      res['items'][0]['volumeInfo']['authors'] || '';

    this.livro.anoEdicao = res['items'][0]['volumeInfo']['publishedDate'];
    this.livro.numeroPaginas = res['items'][0]['volumeInfo']['pageCount'];
    this.livro.descricao = res['items'][0]['volumeInfo']['description'];

    if (res['items'][0]['volumeInfo']['imageLinks']) {
      this.livro.thumbCapa = res['items'][0]['volumeInfo']['imageLinks']['thumbnail'];
    }
  }

  searchGoogleInfo() {
    if (this.livro.isbn) {
      this._googleApi.searchByISBN(this.livro.isbn)
        .subscribe(res => {
          this.getBookInfos(res);
        }, err => {
          console.error(err);
        });
    } else {
      if (this.livro.titulo) {
        const bookQuery =
          this.livro.titulo ? 'title:' + this.livro.titulo : '' +
            this.livro.autor ? 'author:' + this.livro.autor : '' +
              this.livro.editora ? 'publisher:' + this.livro.editora : '';

        this._googleApi.searchBooksByQuery(bookQuery)
          .subscribe(res => {
            console.log(res);
            this.getBookInfos(res);
          }, err => {
            console.error(err);
          });
      }
    }
  }

  backToLastPage() {
    this._location.back();
  }

  validate() {
    if (!this.livro.titulo || !this.livro.autores.length) {
      this._notifierService.notify('error', 'Preencha todos os campos obrigatórios!');
      return false;
    }
    return true;
  }

  saveLivro() {

    if (!this.validate()) {
      return;
    }

    this.livro.publicos = this.livro.publicos ? this.livro.publicos : [];
    this.livro.temas = this.livro.temas ? this.livro.temas : [];

    this.livro.indices = {
      temas: this.livro.temas ? this.livro.temas.map( tema => tema.descricao ) : [],
      publicos: this.livro.publicos ? this.livro.publicos.map( publico => publico.descricao ) : []
    };

    this._spinnerService.show();

    if (this.livro && this.livro.id) {
      this._livrosService.update(this.livro).then(res => {
        this._spinnerService.hide();
        this._notifierService.notify('success', 'Livro alterado com sucesso!');
        this.backToLastPage();
      });
    } else {
      this._livrosService.add(this.livro)
        .then(() => {
          this._spinnerService.hide();
          this._notifierService.notify('success', 'Livro incluído com sucesso!');
          this.backToLastPage();
        });
    }
  }

}
