import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';


@Injectable()
export class GoogleBookApiService {

  constructor(private http: HttpClient) { }

  searchBooks(search) {
    const encodedURI = encodeURI('https://www.googleapis.com/books/v1/volumes?q=' + search + '&maxResults=12');
    return this.http.get(encodedURI);
     // .map((response: Response) => response.json());
  }

  searchBooksByQuery(query) {
    const encodedURI =
      encodeURI(`https://www.googleapis.com/books/v1/volumes?q=${query}&maxResults=6`);
    return this.http.get(encodedURI);
  }

  searchByISBN(isbn) {
    const encodedURI = encodeURI('https://www.googleapis.com/books/v1/volumes?q=isbn:' + isbn + '&maxResults=1');
    return this.http.get(encodedURI);
    //  .map((response: Response) => response.json());
  }
}
