import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NotifierService } from 'angular-notifier';
import { AngularFireStorage } from 'angularfire2/storage';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LivrosReviewFormComponent } from './livros-review-form/livros-review-form.component';
import { AuthService } from 'src/app/shared/services/auth.service';
import { BibliotecaService } from '../biblioteca.service';

@Component({
  selector: 'app-livros-view',
  templateUrl: './livros-view.component.html',
  styleUrls: ['./livros-view.component.scss']
})
export class LivrosViewComponent {

  public livro: any = {
    titulo: '',
    colecao: '',
    autores: [],
    isbn: '',
    editora: '',
    anoEdicao: '',
    numeroPaginas: 0,
    descricao: '',
    thumbCapa: '',
    publicos: [],
    reviews: []
  };

  imageURL = '/assets/images/no-book-bg.png';
  reviewCounter = 0;

  constructor(private _livrosService: BibliotecaService, private _notifierService: NotifierService,
    private _authService: AuthService, private _spinnerService: Ng4LoadingSpinnerService, private modalService: NgbModal,
    private _activateRoute: ActivatedRoute, private storage: AngularFireStorage) {

    this._activateRoute.params.subscribe(
      params => {

        if (params['id']) {
          this._spinnerService.show();
          this._livrosService.getLivro(params['id']).subscribe(res => {
            this.livro = res;
            this.livro.id = params['id'];

            if (!this.livro.reviews) {
              this.livro.reviews = [];
            }

            this.livro.reviews = this.livro.reviews.map((review, i) => {
              review['notaNumbers'] = Array(review.nota).fill(0).map((x, i) => i);
              return review;
            });

            this.storage.ref(this.livro.thumbCapa).getDownloadURL()
              .subscribe(url => {
                this.imageURL = url;
              });

            this._spinnerService.hide();
          });
        }
      });

  }

  getPublicos(livro) {
    if (livro.publicos && livro.publicos.length > 0) {
      return livro.publicos.map(p => `<span class="badge badge-pill badge-outline-primary mr-1"
        style="font-size: 1em;">${ p.text}</span>`).join('');
    }

    return '';
  }

  openPopupReview() {
    const modalRef = this.modalService.open(LivrosReviewFormComponent);
    modalRef.componentInstance.livro = this.livro;
  }

  loggedUserReviewed(): boolean {
    if (!this.livro.reviews) {
      return false;
    }

    return this.livro.reviews.find(r => r.usuario.uid === this._authService.loggedUser.uid);
  }

  isFromLoggedUser(review): boolean {
    return review.usuario.uid === this._authService.loggedUser.uid;
  }

  removeReview(review) {
    this._livrosService.removeReview(review, this.livro);
  }

  getRateLabel(nota) {
    switch (nota) {
      case 1:
        return 'Péssimo';
      case 2:
        return 'Ruim';
      case 3:
        return 'Regular';
      case 4:
        return 'Bom';
      case 5:
        return 'Excelente';
      default:
        return '';
    }
  }


}
