import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotifierService } from 'angular-notifier';
import { BibliotecaService } from '../../biblioteca.service';

@Component({
  selector: 'app-livros-review-form',
  templateUrl: './livros-review-form.component.html',
  styles: [`
    .star {
      font-size: 2.5rem;
      color: #b0c4de;
    }
    .filled {
      color: #1e90ff;
    }
    .bad {
      color: #deb0b0;
    }
    .filled.bad {
      color: #ff1e1e;
    }
  `]
})
export class LivrosReviewFormComponent implements OnInit {

  @Input() livro;

  currentRate = 0;
  review = '';

  constructor(public activeModal: NgbActiveModal, private _livrosService: BibliotecaService,
    private _authService: AuthService, private _notifierService: NotifierService) { }

  ngOnInit() {
  }

  getRateLabel() {
    switch (this.currentRate) {
      case 1:
        return 'Péssimo';
      case 2:
        return 'Ruim';
      case 3:
        return 'Regular';
      case 4:
        return 'Bom';
      case 5:
        return 'Excelente';
      default:
        return '';
    }
  }

  saveReview() {
    const reviewObj = {
      nota: this.currentRate,
      comentario: this.review,
      createdAt: new Date(),
      usuario: this._authService.loggedUser
    };
    this._livrosService.addReview(reviewObj, this.livro)
      .then(res => {
        this._notifierService.notify('success', 'Avaliação registrada com sucesso!');
        this.activeModal.close('Review saved');
      });
  }

}
