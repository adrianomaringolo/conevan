import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivrosReviewFormComponent } from './livros-review-form.component';

describe('LivrosReviewFormComponent', () => {
  let component: LivrosReviewFormComponent;
  let fixture: ComponentFixture<LivrosReviewFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivrosReviewFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivrosReviewFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
