import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivrosThumbComponent } from './livros-thumb.component';

describe('LivrosThumbComponent', () => {
  let component: LivrosThumbComponent;
  let fixture: ComponentFixture<LivrosThumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivrosThumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivrosThumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
