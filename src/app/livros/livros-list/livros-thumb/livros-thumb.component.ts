import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';
import { ListHelper } from 'src/app/shared/helpers/list.helper';
import { AuthService } from 'src/app/shared/services/auth.service';
import * as moment from 'moment';

@Component({
  selector: '[app-livros-thumb]',
  templateUrl: './livros-thumb.component.html',
  styleUrls: ['./livros-thumb.component.scss']
})
export class LivrosThumbComponent extends ListHelper implements OnInit {

  @Input() livro;
  @Output() removeEvent: EventEmitter<any> = new EventEmitter();
  @Output() editEvent: EventEmitter<any> = new EventEmitter();

  nota = null;

  imageURL = '/assets/images/no-book-bg.png';

  constructor(private storage: AngularFireStorage, _authService: AuthService) {
    super(_authService);
  }

  ngOnInit() {
    if (this.livro && this.livro.thumbCapa) {
      this.storage.ref(this.livro.thumbCapa).getDownloadURL()
        .subscribe(url => {
          this.imageURL = url;
        });
    }
    this.nota = this.getLivroNota();
  }

  isRecent(): boolean {
    const createDate = new Date(this.livro.createdAt['seconds'] * 1000);
    return moment(new Date()).diff(createDate, 'days') < 31;
  }

  getPublicos() {
    if (this.livro.publicos && this.livro.publicos.length > 0) {
      return this.livro.publicos.map(p => `<span class="badge badge-pill badge-outline-primary mr-1"
        style="font-size: 1em;">${ p.text }</span>`).join('') ;
    }

    return '';
  }

  getTombosExemplares() {
    return this.livro ? this.livro['exemplaresAdicionais'].map(e => e.tombo).sort().join(', ') : '';
  }

  remove() {
    if (!this.removeEvent) {
      this.removeEvent.emit(this.livro.id);
    }
  }

  private getLivroNota() {
    if (this.livro.reviews) {
      return this.livro.reviews.map(r => r.nota).reduce( ( p, c ) => p + c, 0 ) / this.livro.reviews.length;
    }
    return null;
  }

  public getClassNota() {
    if (this.nota == null) {
      return '';
    }

    if (this.nota <= 1) {
      return 'danger';
    }

    if (this.nota <= 2) {
      return 'warning';
    }

    if (this.nota <= 3) {
      return 'secondary';
    }

    if (this.nota <= 4) {
      return 'primary';
    }

    return 'success';
  }
}
