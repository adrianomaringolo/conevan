import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivrosManagementComponent } from './livros-management.component';

describe('LivrosManagementComponent', () => {
  let component: LivrosManagementComponent;
  let fixture: ComponentFixture<LivrosManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivrosManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivrosManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
