import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NotifierService } from 'angular-notifier';
import { TiposSocios, BibliotecaService } from '../../biblioteca.service';

@Component({
  selector: 'app-socios-form',
  templateUrl: './socios-form.component.html'
})
export class SociosFormComponent implements OnInit {

  novoTema = '';
  temas: any[];

  @Input() socios: any[] = [];
  @Input() socio: any = {};

  TiposSocios = TiposSocios;

  constructor(public activeModal: NgbActiveModal,
    private _livrosService: BibliotecaService, private _notifierService: NotifierService,
    private _spinnerService: Ng4LoadingSpinnerService) {
    this._spinnerService.show();

    // this._livrosService.temas.subscribe(res => {
    //   this.temas = res;
    //   this._spinnerService.hide();
    // });
  }

  remove(key) {
    this._livrosService.removeTema(key)
      .then(() => {
        this._notifierService.notify('success', 'Tema removido com sucesso!');
      });
  }

  add() {
    if (!this.socio.nome || !this.socio.email || !this.socio.telefone || !this.socio.grupo) {
      this._notifierService.notify('error', 'Preencha os campos obrigatórios (*)');
      return;
    }

    this._spinnerService.show();

    let currentNumber = 1;

    if (this.socios && this.socios.length > 0) {
      currentNumber = this.socios.sort((a, b) => a.numero - b.numero)[this.socios.length - 1]['numero'] + 1;
    }

    if (!this.socio.id) {
      this._livrosService.addSocio({ numero: currentNumber, ...this.socio })
        .then(() => {
          this._spinnerService.hide();
          this.activeModal.close();
          this._notifierService.notify('success', 'Sócio incluído com sucesso!');
        });
      } else {
        this._livrosService.updateSocio(this.socio)
        .then(() => {
          this._spinnerService.hide();
          this.activeModal.close();
          this._notifierService.notify('success', 'Sócio editado com sucesso!');
        });
      }

  }

  ngOnInit() {
  }

}
