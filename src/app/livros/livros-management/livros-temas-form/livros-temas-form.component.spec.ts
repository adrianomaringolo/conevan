import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivrosTemasFormComponent } from './livros-temas-form.component';

describe('LivrosTemasFormComponent', () => {
  let component: LivrosTemasFormComponent;
  let fixture: ComponentFixture<LivrosTemasFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivrosTemasFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivrosTemasFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
