import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NotifierService } from 'angular-notifier';
import { BibliotecaService } from '../../biblioteca.service';

@Component({
  selector: 'app-livros-temas-form',
  templateUrl: './livros-temas-form.component.html',
  styleUrls: ['./livros-temas-form.component.scss']
})
export class LivrosTemasFormComponent implements OnInit {

  novoTema = '';
  temas: any[];

  constructor(public activeModal: NgbActiveModal,
    private _livrosService: BibliotecaService, private _notifierService: NotifierService,
    private _spinnerService: Ng4LoadingSpinnerService) {
    this._spinnerService.show();

    // this._livrosService.temas.subscribe(res => {
    //   this.temas = res;
    //   this._spinnerService.hide();
    // });
  }

  remove(key) {
    this._livrosService.removeTema(key)
      .then(() => {
        this._notifierService.notify('success', 'Tema removido com sucesso!');
      });
  }

  add() {
    if (!this.novoTema || this.temas.findIndex(tema => tema.descricao.toUpperCase() === this.novoTema.toUpperCase()) >= 0) {
      return;
    }

    this._spinnerService.show();

      this._livrosService.addTema({ descricao: this.novoTema })
        .then(() => {
          this._spinnerService.hide();
          this.novoTema = '';
          this._notifierService.notify('success', 'Tema incluído com sucesso!');
        });

  }

  ngOnInit() {
  }

}
