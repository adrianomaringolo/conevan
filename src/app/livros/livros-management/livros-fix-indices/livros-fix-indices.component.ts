import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BibliotecaService } from '../../biblioteca.service';

@Component({
  selector: 'app-livros-fix-indices',
  templateUrl: './livros-fix-indices.component.html',
  styleUrls: ['./livros-fix-indices.component.scss']
})
export class LivrosFixIndicesComponent {

  @Input() livros: any[] = [];
  novoTema = '';
  temas: any[];

  processRunning = false;

  fixNotasStarted = false;
  livrosProcessed = 0;

  constructor(public activeModal: NgbActiveModal) {
  }

  getProgressStatus() {
    // tslint:disable-next-line:radix
    return parseInt( ((this.livrosProcessed / this.livros.length) * 100).toString() );
  }

  startFixes() {
    this.processRunning = true;
    this.startFixNotas();
  }

  startFixNotas() {
    // this.fixNotasStarted = true;
    // this.livrosProcessed = 0;
    // this.livros.forEach(livro => {
    //   this._livrosService.fixNotas(livro).then(
    //     () => {
    //       this.livrosProcessed++;
    //       console.log(this.livrosProcessed);
    //     }
    //   );
    // });
  }

}
