import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AlunosService } from 'src/app/alunos/alunos.service';
import { EvangelizadoresService } from 'src/app/evangelizadores/evangelizadores.service';
import { BibliotecaService, TiposSocios } from '../../biblioteca.service';

@Component({
  selector: 'app-socios-import-existent',
  templateUrl: './socios-import-existent.component.html'
})
export class SociosImportExistentComponent {

  alunos: any[] = [];
  alunosProcessed = 0;

  evangelizadores: any[] = [];
  evangelizadoresProcessed = 0;

  currentNumber = 0;

  @Input() socios: any[];

  processRunning = false;
  importAlunosStarted = false;
  importEvangelizadoresStarted = false;

  constructor(public activeModal: NgbActiveModal, private _alunosService: AlunosService,
    private _livrosService: BibliotecaService, private _evangelizadoresService: EvangelizadoresService) {

    this._alunosService.changeTurma('');
    this._alunosService.alunos.subscribe(res => {
      this.alunos = res;
    });

    this._evangelizadoresService.evangelizadores.subscribe(res => {
      this.evangelizadores = res;
    });
  }

  getProgressStatus(processed, collecion) {
    // tslint:disable-next-line:radix
    return parseInt(((processed / collecion.length) * 100).toString());
  }

  startFixes() {
    this.processRunning = true;
    this.startImportAlunos();
  }

  startImportAlunos() {
    this.importAlunosStarted = true;
    this.alunosProcessed = 0;

    if (this.socios && this.socios.length > 0) {
      this.currentNumber = this.socios.sort((a, b) => a.numero - b.numero)[this.socios.length - 1]['numero'] + 1;
    }

    this.alunos.sort((a, b) => a.nome - b.nome).forEach(aluno => {
      const foundIndex = this.socios ?
        this.socios.findIndex(socio => socio.nome === aluno.nome + ' ' + aluno.sobrenome) : -1;

      if (foundIndex > -1) {
        this.alunosProcessed++;
        return;
      }

      this._livrosService.addSocio({
        numero: this.currentNumber++,
        nome: aluno.nome + ' ' + aluno.sobrenome,
        email: aluno.responsavel ? aluno.responsavel.email : '',
        telefone: aluno.responsavel ? aluno.responsavel.telefone : '',
        grupo: TiposSocios.ALUNOS,
        obs: aluno.turma
      }).then(() => {
          this.alunosProcessed++;

          if (this.alunosProcessed === this.alunos.length) {
            this.startImportEvangelizadores();
          }
        }
      );
    });
  }

  startImportEvangelizadores() {
    this.importEvangelizadoresStarted = true;
    this.evangelizadoresProcessed = 0;

    this.evangelizadores.forEach(evangelizador => {

      const foundIndex = this.socios ?
        this.socios.findIndex(socio => socio.nome === evangelizador.nome + ' ' + evangelizador.sobrenome) : -1;

      if (foundIndex > -1) {
        this.evangelizadoresProcessed++;
        return;
      }

      this._livrosService.addSocio({
        numero: this.currentNumber++,
        nome: evangelizador.nome + ' ' + evangelizador.sobrenome,
        email: evangelizador.email,
        telefone: evangelizador.telefone,
        grupo: TiposSocios.EVANGELIZADORES,
        obs: ''
      }).then(
        () => {
          this.evangelizadoresProcessed++;
          if (this.evangelizadoresProcessed === this.evangelizadores.length) {
            this.processRunning = false;
          }
        }
      );
    });
  }

}
