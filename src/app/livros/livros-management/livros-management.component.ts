import { Component, OnInit } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { ListHelper } from 'src/app/shared/helpers/list.helper';
import { LivrosTemasFormComponent } from './livros-temas-form/livros-temas-form.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LivrosFixIndicesComponent } from './livros-fix-indices/livros-fix-indices.component';
import { SociosImportExistentComponent } from './socios-import-existent/socios-import-existent.component';
import { SociosFormComponent } from './socios-form/socios-form.component';
import { NotifierService } from 'angular-notifier';
import { Subscription } from 'rxjs';
import { TiposSocios, BibliotecaService } from '../biblioteca.service';

@Component({
  selector: 'app-livros-management',
  templateUrl: './livros-management.component.html',
  styleUrls: ['./livros-management.component.scss']
})
export class LivrosManagementComponent extends ListHelper {

  livros: any[];
  livrosBackup: any[];

  TiposSocios = TiposSocios;

  socios: any[];
  sociosBackup: any[];
  selectedGroup = TiposSocios.ALUNOS;

  subscriptionSocios: Subscription;

  statusLivros = Object.freeze({
    ativo: 'ativo',
    perdido: 'perdido',
    doado: 'doado',
    sem_uso: 'sem_uso'
  });

  constructor(private _livrosService: BibliotecaService, private modalService: NgbModal,
    private _spinnerService: Ng4LoadingSpinnerService, private _router: Router,
    _authService: AuthService, private _notifierService: NotifierService) {

    super(_authService);

    this._spinnerService.show();

    this.livrosBackup = JSON.parse(localStorage.getItem('all-livros'));

    // if (this.livrosBackup) {
    //   this.livros = JSON.parse(JSON.stringify(this.livrosBackup));
    //   this._spinnerService.hide();
    // } else {
      this._livrosService.changeFilters('tombo-desc', '');
      this._livrosService.livros.subscribe(res => {
        this.livros = res;
        this.livrosBackup = JSON.parse(JSON.stringify(this.livros));

        localStorage.setItem('all-livros', JSON.stringify(this.livrosBackup));
        this._spinnerService.hide();
      });
    // }

    this.findSocios();
  }

  findSocios() {
    this.subscriptionSocios = this._livrosService.socios.subscribe(res => {
      this.socios = res;
      this.sociosBackup = JSON.parse(JSON.stringify(this.socios));
      this.changeSelectedGrupo(this.selectedGroup || TiposSocios.ALUNOS);
      this._spinnerService.hide();
    });
  }

  updateCollection() {
    localStorage.removeItem('all-livros');

    this._livrosService.changeFilters('tombo-desc', '');
      this._livrosService.livros.subscribe(res => {
        this.livros = res;
        this.livrosBackup = JSON.parse(JSON.stringify(this.livros));

        localStorage.setItem('all-livros', JSON.stringify(this.livrosBackup));
        this._spinnerService.hide();
      });
  }

  getBookingProgress() {
    const okBooks = this.livros.filter(l => l.cadastroCompleto && l.status === this.statusLivros.ativo).length;
    const totalBooks = this.livros.filter(l => l.status === this.statusLivros.ativo).length;
    return `${okBooks} / ${totalBooks} (${ (okBooks/totalBooks * 100).toFixed(0) }%)`
  }

  changeSelectedGrupo(tipo) {
    this.selectedGroup = tipo;
    this.socios = JSON.parse(JSON.stringify(this.sociosBackup)).filter(socio => socio.grupo === tipo);
  }

  remove(key) {
    this._livrosService.remove(key);
  }

  edit(id): void {
    this._router.navigate([, id]);
  }

  getStatusLivro(livro) {
    switch (livro.status) {
      case (this.statusLivros.ativo):
        return '<span class="badge badge-success">Ativo</span>';
      case (this.statusLivros.perdido):
        return '<span class="badge badge-danger">Perdido</span>';
      case (this.statusLivros.doado):
        return '<span class="badge badge-secondary">Doado</span>';
      case (this.statusLivros.ativo):
        return '<span class="badge badge-dark">Sem condições de uso</span>';
      default:
        return '';
    }
  }

  openPopupTemas() {
    const modalRef = this.modalService.open(LivrosTemasFormComponent);
  }

  openPopupFix() {
    const modalRef = this.modalService.open(LivrosFixIndicesComponent);
    modalRef.componentInstance.livros = JSON.parse(JSON.stringify(this.livros));
  }

  openPopupSociosImport() {
    const modalRef = this.modalService.open(SociosImportExistentComponent);
    modalRef.componentInstance.socios = JSON.parse(JSON.stringify(this.sociosBackup));
    this.subscriptionSocios.unsubscribe();
  }

  openPopupSociosForm(socio?) {
    const modalRef = this.modalService.open(SociosFormComponent);
    modalRef.componentInstance.socios = JSON.parse(JSON.stringify(this.sociosBackup));

    if (socio) {
      modalRef.componentInstance.socio = socio;
    }
  }

  getSocioTipoClass(tipo) {
    switch (tipo) {
      case TiposSocios.ALUNOS:
        return 'badge-primary';
      case TiposSocios.EVANGELIZADORES:
        return 'badge-info';
      case TiposSocios.PAIS:
        return 'badge-danger';
      case TiposSocios.OUTROS:
        return 'badge-dark';
    }
    return '';
  }

  removeSocio(key) {
    this._spinnerService.show();
    this._livrosService.removeSocio(key).then(() => {
      this._notifierService.notify('success', 'Sócio removido com sucesso!');
      this._spinnerService.hide();
    });
  }

}
