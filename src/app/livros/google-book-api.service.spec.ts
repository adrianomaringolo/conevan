import { TestBed } from '@angular/core/testing';

import { GoogleBookApiService } from './google-book-api.service';

describe('GoogleBookApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GoogleBookApiService = TestBed.get(GoogleBookApiService);
    expect(service).toBeTruthy();
  });
});
