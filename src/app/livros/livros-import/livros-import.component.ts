import { Component, OnInit } from '@angular/core';
import { ListHelper } from 'src/app/shared/helpers/list.helper';
import { AuthService } from 'src/app/shared/services/auth.service';
import { BibliotecaService } from '../biblioteca.service';



@Component({
  selector: 'app-livros-import',
  templateUrl: './livros-import.component.html',
  styleUrls: ['./livros-import.component.scss']
})
export class LivrosImportComponent extends ListHelper {

  file: any;
  livros: any[];
  livrosProcessed = 0;
  resultStatus = {};

  processStarted = false;

  statusLivros = Object.freeze({
    none: '<span class="badge badge-dark">Não iniciado</span>',
    started: '<span class="badge badge-info">Iniciado</span>',
    stopped: '<span class="badge badge-dark">Parado</span>',
    ignored: '<span class="badge badge-secondary">Ignorado</span>',
    imported: '<span class="badge badge-success">Importado</span>',
    duplicated: '<span class="badge badge-warning">Item já existe</span>',
    error: '<span class="badge badge-danger">Houve erro ao criar o item</span>'
  });

  constructor(_authService: AuthService, private _livrosService: BibliotecaService) {
    super(_authService);
  }

  changeFile(event) {
    this.file = event.target.files[0];
  }

  convertFile() {

    const reader = new FileReader();
    reader.onload = () => {
      // convert text to json here
      this.livros = this.csvJSON(reader.result);
      this.livros.map(l => l.tombo = this._livrosService.pad(l.tombo));
      this.livros.forEach(l => {
        this.resultStatus[l.tombo] = this.statusLivros.none;
      });
    };
    reader.readAsText(this.file);
  }

  public csvJSON(csv) {
    const lines = csv.split('\n');
    const result = [];
    const headers = lines[0].split(';');

    for (let i = 1; i < lines.length; i++) {
      const obj = {};
      const currentline = lines[i].split(';');
      for (let j = 0; j < headers.length; j++) {
        obj[headers[j].toLowerCase()] = currentline[j];
      }
      result.push(obj);
    }
    return result;
  }

  getProgressStatus() {
    // tslint:disable-next-line:radix
    return parseInt( ((this.livrosProcessed / this.livros.length) * 100).toString() );
  }

  startImport() {
    this.processStarted = true;
    this.livrosProcessed = 0;
    this.livros.forEach(livro => {
      this.resultStatus[livro.tombo] = this.statusLivros.started;
      this.saveLivro(livro);
    });
  }

  saveLivro(newlivro) {

    if (newlivro.tombo && newlivro.titulo) {
      this._livrosService.getByTombo(newlivro.tombo).subscribe((res) => {
        console.log(newlivro.tombo);
        if (!res.docs[0]) {
          const livroToAdd = { ...newlivro };
          livroToAdd.autores = livroToAdd.autores.split(',');
          delete livroToAdd.status;

          this._livrosService.add(livroToAdd)
            .then(() => {
              this.resultStatus[livroToAdd.tombo] = this.statusLivros.imported;
              this.livrosProcessed++;
            })
            .catch(_ => {
              this.resultStatus[livroToAdd.tombo] = this.statusLivros.error;
              this.livrosProcessed++;
            });
        } else {
          this.resultStatus[newlivro.tombo] = this.statusLivros.duplicated;
          this.livrosProcessed++;
        }


      });
    } else {
      this.resultStatus[newlivro.tombo] = this.statusLivros.ignored;
      this.livrosProcessed++;
    }
  }

}
