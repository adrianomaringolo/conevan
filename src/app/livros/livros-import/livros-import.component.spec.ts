import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivrosImportComponent } from './livros-import.component';

describe('LivrosImportComponent', () => {
  let component: LivrosImportComponent;
  let fixture: ComponentFixture<LivrosImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivrosImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivrosImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
