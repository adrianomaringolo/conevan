import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LivrosListComponent } from './livros-list/livros-list.component';
import { LivrosFormComponent } from './livros-form/livros-form.component';
import { LivrosImportComponent } from './livros-import/livros-import.component';
import { LivrosViewComponent } from './livros-view/livros-view.component';
import { FormsModule } from '@angular/forms';
import { LivrosThumbComponent } from './livros-list/livros-thumb/livros-thumb.component';
import { SharedModule } from '../shared/shared.module';
import { LivrosManagementComponent } from './livros-management/livros-management.component';
import { LivrosTemasFormComponent } from './livros-management/livros-temas-form/livros-temas-form.component';
import { LivrosReviewFormComponent } from './livros-view/livros-review-form/livros-review-form.component';
import { LivrosFixIndicesComponent } from './livros-management/livros-fix-indices/livros-fix-indices.component';
import { SociosImportExistentComponent } from './livros-management/socios-import-existent/socios-import-existent.component';
import { SociosFormComponent } from './livros-management/socios-form/socios-form.component';
import { EmprestimosListComponent } from './emprestimos-list/emprestimos-list.component';

@NgModule({
  declarations: [
    LivrosListComponent,
    LivrosThumbComponent,
    LivrosManagementComponent,
    LivrosFormComponent,
    LivrosImportComponent,
    LivrosViewComponent,
    LivrosTemasFormComponent,
    LivrosReviewFormComponent,
    LivrosFixIndicesComponent,
    SociosImportExistentComponent,
    SociosFormComponent,
    EmprestimosListComponent
  ],
  exports: [
    LivrosListComponent,
    LivrosThumbComponent,
    LivrosManagementComponent,
    LivrosFormComponent,
    LivrosImportComponent,
    LivrosViewComponent
  ],
  entryComponents: [
    LivrosTemasFormComponent,
    LivrosReviewFormComponent,
    LivrosFixIndicesComponent,
    SociosImportExistentComponent,
    SociosFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ]
})
export class LivrosModule { }
