import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as nodemailer from 'nodemailer';
const cors = require('cors')({origin: true});

admin.initializeApp();

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

export const helloWorld = functions.https.onRequest((request, response) => {
  console.log('Hi!');
  response.send("Hello from Conevan!");
});

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'evangelizacao.meplh@gmail.com',
    pass: 'meplh.evangelizacao19'
  }
});

export const sendEmail = functions.https.onRequest((req, res) => {
  cors(req, res, () => {

    // getting dest email by query string
    const dest = req.query.dest;

    const mailOptions = {
      from: 'Evangelização Morada <evangelizacao.meplh@gmail.com>', // Something like: Jane Doe <janedoe@gmail.com>
      to: dest,
      subject: 'I\'M A PICKLE!!!', // email subject
      html: `<p style="font-size: 16px;">Pickle Riiiiiiiiiiiiiiiick!!</p>
              <br />
              <img src="https://images.prod.meredith.com/product/fc8754735c8a9b4aebb786278e7265a5/1538025388228/l/rick-and-morty-pickle-rick-sticker" />
          ` // email content in HTML
    };

    // returning result
    return transporter.sendMail(mailOptions, (erro, info) => {
      if (erro) {
        return res.send(erro.toString());
      }
      return res.send('Sended');
    });
  });
});

export const sendEmprestimoEmail = functions.https.onRequest(async (req, res) => {
  cors(req, res, async () => {

    const emprestimo = (await admin.firestore().collection('emprestimos').doc(req.query.idEmprestimo).get()).data();

    const dataEmprestimo = new Date(emprestimo.dataEmprestimo);
    const dataEmprestimoStr = `${dataEmprestimo.getDate()}/${dataEmprestimo.getMonth() + 1}/${dataEmprestimo.getFullYear()}`;

    const dataDevolver = new Date(emprestimo.dataDevolver);
    const dataDevolverStr = `${dataDevolver.getDate()}/${dataDevolver.getMonth() + 1}/${dataDevolver.getFullYear()}`;

    const mailOptions = {
      from: 'Evangelização Morada <evangelizacao.meplh@gmail.com>', // Something like: Jane Doe <janedoe@gmail.com>
      to: emprestimo.socio.email,
      bcc: 'Evangelização Morada <evangelizacao.meplh@gmail.com>',
      subject: '[Biblioteca] Empréstimo do livro - #' + emprestimo.livro.tombo, // email subject
      html: `
          <div style="background: linear-gradient(120deg,#00e4d0,#5983e8); padding: 20px;">
            <div style="max-width: 600px; margin: 25px auto; background-color: white; padding: 25px;">
              Olá,<br/>
              foi feito um empréstimo na Biblioteca da Evangelização no dia ${dataEmprestimoStr}.

              <p>Seguem os dados para acompanhamento:</p>

              <p style="font-size: 20px;"><b>Sócio</b>: ${emprestimo.socio.nome}</p>
              <div style="display: flex">
                <img src="${emprestimo.livro.thumbCapa}" style="width: 150px">
                <div style="padding-left: 20px; font-size: 16px; min-width: 150px;">
                  <b>Tombo:</b> #${emprestimo.livro.tombo} <br/>
                  <b>Título:</b> ${emprestimo.livro.titulo} <br/>
                  <b>Autor(es):</b> ${emprestimo.livro.autores}
                </div>
              </div>

              <div style="border: 1px solid red; background-color: navajowhite; padding: 0 15px; margin-top: 25px;">
                <p style="font-size: 16px">O livro deve ser devolvido até o dia <b>${dataDevolverStr}</b>.</p>
                <p>Caso queira renovar por mais ${emprestimo.diasEmprestimo} dias, por favor entre em contato.</p>
              </div>

              <div style="border: 1px solid gray; background-color: lightgray; padding: 0 15px; margin-top: 25px;">
                <p><strong>CUIDADOS COM O LIVRO:</strong></p>
                <p>Esse livro é um patrimônio da Evangelização e vai ser usado por muitas crianças, por favor, não rasure. Cuide para que tenhamos esse
                patrimônio por muito tempo.</p>
                <p>Em caso de perda ou dano (que inutilize o item), pedimos que o responsável reponha o livro referido com outro exemplar do mesmo título (ou outro a ser acordado
                com a Equipe da Biblioteca da Evangelização).
              </div>

              <p>
              Atenciosamente, <br/>
              Equipe da Biblioteca da Evangelização
              </p>

              <div style="color: gray; font-size: 12px; border-top: 1px solid gray; padding-top: 5px; font-style: italic;">Este é um email automático, por favor não responda</div>
            </div>
          </div>
          ` // email content in HTML
    };

    // returning result
    return transporter.sendMail(mailOptions, (erro, info) => {
      if (erro) {
        return res.send(erro.toString());
      }
      return res.send('Sended');
    });
  });
});

